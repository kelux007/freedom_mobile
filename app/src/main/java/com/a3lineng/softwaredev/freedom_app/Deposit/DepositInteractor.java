package com.a3lineng.softwaredev.freedom_app.Deposit;

import android.content.DialogInterface;

import im.delight.android.location.SimpleLocation;

public interface DepositInteractor {

    void deposit(String accountNumber, String amount, String narration, String customerPin, String agentPin, OnDepositFinishedListener listener);

    void nameEnquiry(String accountName, SimpleLocation location, OnDepositFinishedListener listener);

    interface OnDepositFinishedListener{
        void onError(String title, String message);

        void onSuccess(String title, String message, DialogInterface.OnClickListener yeslistener);

        void onNameEnquirySuccess(String accountName);

        void onUpdateProgressMessage(String message);

        void onWrongPinEntered(String title, String message, DialogInterface.OnClickListener cancel, DialogInterface.OnClickListener retry);

        void onStartDeposit(String message);

        void destroyFragment();

        void onDisplayAlert(String title, String message, DialogInterface.OnClickListener yeslistener, DialogInterface.OnClickListener nolistener);
    }
}
