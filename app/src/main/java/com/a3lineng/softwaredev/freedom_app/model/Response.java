package com.a3lineng.softwaredev.freedom_app.model;

public class Response {
    private long id;
    private String respCode;
    private String respDescription;
    private String accountBalance;
    private String accountNumber;

    public Response(long id, String respCode, String respDescription, String accountBalance, String accountNumber) {
        this.id = id;
        this.respCode = respCode;
        this.respDescription = respDescription;
        this.accountBalance = accountBalance;
        this.accountNumber = accountNumber;
    }

    public Response(long id, String respCode, String respDescription) {
        this.id = id;
        this.respCode = respCode;
        this.respDescription = respDescription;
    }

    public Response() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespDescription() {
        return respDescription;
    }

    public void setRespDescription(String respDescription) {
        this.respDescription = respDescription;
    }

    public String getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(String accountBalance) {
        this.accountBalance = accountBalance;
    }
}
