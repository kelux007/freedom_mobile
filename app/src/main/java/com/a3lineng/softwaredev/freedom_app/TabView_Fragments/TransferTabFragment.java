package com.a3lineng.softwaredev.freedom_app.TabView_Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.a3lineng.softwaredev.freedom_app.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransferTabFragment extends Fragment {


    public TransferTabFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transfer_tab, container, false);
    }

}
