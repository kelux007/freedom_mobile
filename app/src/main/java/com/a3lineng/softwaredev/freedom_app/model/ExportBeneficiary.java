package com.a3lineng.softwaredev.freedom_app.model;

public class ExportBeneficiary {
    public static final String TABLE_BENEFICIARY = "beneficiaries";
    public static final String COLUMN_BEN_ID = "id";
    public static final String COLUMN_BEN_NAME = "ben_name";
    public static final String COLUMN_BEN_GENDER = "ben_gender";
    public static final String COLUMN_BEN_REF_NUM = "ben_ref_num";
    public static final String COLUMN_BEN_AGE = "ben_age";
    public static final String COLUMN_BEN_PHONE = "ben_phone";
    public static final String COLUMN_ALTERNATE_NAME = "alt_name";
    public static final String COLUMN_ALTERNATE_GENDER = "alt_gender";
    public static final String COLUMN_ALTERNATE_REF_NUM = "alt_ref_num";
    public static final String COLUMN_ALTERNATE_AGE = "alt_age";
    public static final String COLUMN_ALTERNATE_PHONE = "alt_phone";
    public static final String COLUMN_LGA = "lga";
    public static final String COLUMN_STATE = "state";
    public static final String COLUMN_WARD = "ward";
    public static final String COLUMN_COMMUNITY = "community";
    public static final String COLUMN_BEN_PICTURE = "ben_pic";
    public static final String COLUMN_ALTERNATE_PICTURE = "alt_pic";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_CITY = "city";
    public static final String COLUMN_HOUSE_HOLD_NO = "house_hold_no";
    public static final String CREATE_TABLE_BENEFICIARY = "CREATE TABLE " + TABLE_BENEFICIARY + "("
            + COLUMN_BEN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_BEN_NAME + " TEXT,"
            + COLUMN_BEN_GENDER + " TEXT,"
            + COLUMN_BEN_REF_NUM + " TEXT,"
            + COLUMN_BEN_AGE + " DATETIME,"
            + COLUMN_BEN_PHONE + " TEXT,"
            + COLUMN_BEN_PICTURE + " TEXT,"
            + COLUMN_ALTERNATE_NAME + " TEXT,"
            + COLUMN_ALTERNATE_GENDER + " TEXT,"
            + COLUMN_ALTERNATE_REF_NUM + " TEXT,"
            + COLUMN_ALTERNATE_AGE + " DATETIME,"
            + COLUMN_ALTERNATE_PHONE + " TEXT,"
            + COLUMN_ALTERNATE_PICTURE + " TEXT,"
            + COLUMN_ADDRESS + " TEXT,"
            + COLUMN_CITY + " TEXT,"
            + COLUMN_HOUSE_HOLD_NO + " TEXT,"
            + COLUMN_STATE + " TEXT,"
            + COLUMN_LGA + " TEXT,"
            + COLUMN_WARD + " TEXT,"
            + COLUMN_COMMUNITY + " TEXT"
            + ");";
    private static final String SPACE = " ";
    private String caregiverName;
    private String alternateName;
    private String caregiverGender;
    private String alternateGender;
    private String caregiverRefNum;
    private String alternateRefNum;
    private String caregiverAge;
    private String alternateAge;
    private String caregiverPhone;
    private String alternatePhone;
    private String address;
    private String state;
    private String city;
    private String lga;
    private String ward;
    private String community;
    private String caregiverbase64pic;
    private String alternatebase64pic;
    private String houseHoldNum;

    public ExportBeneficiary() {
    }

    public ExportBeneficiary(Beneficiary beneficiary) {
        this.caregiverName = beneficiary.getFirstName() + SPACE + beneficiary.getMiddleName() + SPACE + beneficiary.getLastName();
        this.alternateName = beneficiary.getAltFirstName() + SPACE + beneficiary.getAltMiddleName() + SPACE + beneficiary.getAltLastName();
        this.caregiverGender = beneficiary.getGender();
        this.alternateGender = beneficiary.getAltGender();
        this.caregiverRefNum = beneficiary.getState() + "/" + beneficiary.getLga() + "/" + beneficiary.getCommunity() + "/" + beneficiary.getHouseHoldNum();
        this.alternateRefNum = beneficiary.getState() + "/" + beneficiary.getLga() + "/" + beneficiary.getCommunity() + "/" + beneficiary.getHouseHoldNum();
        this.caregiverAge = "60";
        this.alternateAge = "40";
        this.caregiverPhone = beneficiary.getPhoneNumber();
        this.alternatePhone = beneficiary.getAltPhone();
        this.address = beneficiary.getAddress();
        this.state = beneficiary.getState();
        this.city = beneficiary.getCity();
        this.lga = beneficiary.getLga();
        this.ward = beneficiary.getWard();
        this.caregiverbase64pic = beneficiary.getPicture();
        this.alternatebase64pic = beneficiary.getAltPicture();
    }

    public String getCaregiverName() {
        return caregiverName;
    }

    public void setCaregiverName(String caregiverName) {
        this.caregiverName = caregiverName;
    }

    public String getAlternateName() {
        return alternateName;
    }

    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public String getCaregiverGender() {
        return caregiverGender;
    }

    public void setCaregiverGender(String caregiverGender) {
        this.caregiverGender = caregiverGender;
    }

    public String getAlternateGender() {
        return alternateGender;
    }

    public void setAlternateGender(String alternateGender) {
        this.alternateGender = alternateGender;
    }

    public String getCaregiverRefNum() {
        return caregiverRefNum;
    }

    public void setCaregiverRefNum(String caregiverRefNum) {
        this.caregiverRefNum = caregiverRefNum;
    }

    public String getAlternateRefNum() {
        return alternateRefNum;
    }

    public void setAlternateRefNum(String alternateRefNum) {
        this.alternateRefNum = alternateRefNum;
    }

    public String getCaregiverAge() {
        return caregiverAge;
    }

    public void setCaregiverAge(String caregiverAge) {
        this.caregiverAge = caregiverAge;
    }

    public String getAlternateAge() {
        return alternateAge;
    }

    public void setAlternateAge(String alternateAge) {
        this.alternateAge = alternateAge;
    }

    public String getCaregiverPhone() {
        return caregiverPhone;
    }

    public void setCaregiverPhone(String caregiverPhone) {
        this.caregiverPhone = caregiverPhone;
    }

    public String getAlternatePhone() {
        return alternatePhone;
    }

    public void setAlternatePhone(String alternatePhone) {
        this.alternatePhone = alternatePhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLga() {
        return lga;
    }

    public void setLga(String lga) {
        this.lga = lga;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    public String getCaregiverBase64Pic() {
        return this.caregiverbase64pic;
    }

    public void setCaregiverBase64Pic(String caregiverBase64Pic) {
        this.caregiverbase64pic = caregiverBase64Pic;
    }

    public String getAlternateBase64Pic() {
        return this.alternatebase64pic;
    }

    public void setAlternateBase64Pic(String alternateBase64Pic) {
        this.alternatebase64pic = alternateBase64Pic;
    }

    public String getHouseHoldNum() {
        return houseHoldNum;
    }

    public void setHouseHoldNum(String houseHoldNum) {
        this.houseHoldNum = houseHoldNum;
    }

    @Override
    public String toString() {
        return "ExportBeneficiary{" +
                "caregiverName='" + caregiverName + '\'' +
                ", alternateName='" + alternateName + '\'' +
                ", caregiverGender='" + caregiverGender + '\'' +
                ", alternateGender='" + alternateGender + '\'' +
                ", caregiverRefNum='" + caregiverRefNum + '\'' +
                ", alternateRefNum='" + alternateRefNum + '\'' +
                ", caregiverAge='" + caregiverAge + '\'' +
                ", alternateAge='" + alternateAge + '\'' +
                ", caregiverPhone='" + caregiverPhone + '\'' +
                ", alternatePhone='" + alternatePhone + '\'' +
                ", address='" + address + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", lga='" + lga + '\'' +
                ", ward='" + ward + '\'' +
                ", community='" + community + '\'' +
                ", caregiverBase64Pic='" + caregiverbase64pic + '\'' +
                ", alternateBase64Pic='" + alternatebase64pic + '\'' +
                ", houseHoldNum='" + houseHoldNum + '\'' +
                '}';
    }
}
