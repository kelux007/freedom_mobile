package com.a3lineng.softwaredev.freedom_app.utilities;

public class Constants {
    public static final int SETUPACTIVITY = 100;
    public static final int CAMERA_PERMISSION_REQUEST = 200;
    public static final String HTTP_POST = "POST";
    public static final String HTTP_GET = "GET";
    public static final String HTTP_PUT = "PUT";
    public static final String CAMERA_PERMISSION = "Manifest.permission.CAMERA";
    public static final int REQUEST_CODE_QR_SCAN = 101;
}
