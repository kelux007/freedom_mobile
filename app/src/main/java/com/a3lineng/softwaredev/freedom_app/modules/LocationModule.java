package com.a3lineng.softwaredev.freedom_app.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import im.delight.android.location.SimpleLocation;

@Module
public class LocationModule {
    private Context context;

    public LocationModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return this.context;
    }

    @Provides
    @Singleton
    SimpleLocation provideSimpleLocation() {
        return new SimpleLocation(context);
    }
}
