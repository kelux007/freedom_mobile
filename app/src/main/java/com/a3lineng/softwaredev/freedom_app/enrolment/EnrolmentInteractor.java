package com.a3lineng.softwaredev.freedom_app.enrolment;

import android.content.DialogInterface;

import com.a3lineng.softwaredev.freedom_app.base_interactors.BaseFinishedListener;
import com.a3lineng.softwaredev.freedom_app.base_interactors.BaseInteractor;

public interface EnrolmentInteractor extends BaseInteractor {
    void enrol(String name, String bvn, String motherMaidenName, String dob, String phone, OnEnrolFinished listener);

    interface OnEnrolFinished extends BaseFinishedListener{
        void onSuccess(String title, String message, DialogInterface.OnClickListener yeslistener);
        void destroyFragment();
    }
}
