package com.a3lineng.softwaredev.freedom_app.enrolment;

import android.content.DialogInterface;

public class EnrolmentPresenterImpl implements EnrolmentPresenter, EnrolmentInteractor.OnEnrolFinished{
    private EnrolmentView enrolmentView;
    private EnrolmentInteractor enrolmentInteractor;

    public EnrolmentPresenterImpl(EnrolmentView enrolmentView, EnrolmentInteractor enrolmentInteractor) {
        this.enrolmentView = enrolmentView;
        this.enrolmentInteractor = enrolmentInteractor;
    }

    @Override
    public void onError(String title, String message) {
        if(enrolmentView != null){
            enrolmentView.hideProgress();
            enrolmentView.alert(title, message);
        }
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onUpdateProgressMessage(String message) {

    }

    @Override
    public void onStart(String message) {
        if(enrolmentView != null){
            enrolmentView.showProgress(message);
        }
    }

    @Override
    public void validateEnrolmentField(String name, String bvn, String motherMaidenName, String dob, String phone) {
        if(enrolmentView != null){
            enrolmentInteractor.enrol(name, bvn, motherMaidenName, dob, phone, this);
        }
    }

    @Override
    public void onDestroy() {
        if(enrolmentView != null){
            enrolmentView = null;
            enrolmentInteractor = null;
        }
    }

    @Override
    public void onSuccess(String title, String message, DialogInterface.OnClickListener yeslistener) {
        if(enrolmentView != null){
            enrolmentView.hideProgress();
            enrolmentView.alert(title, message, yeslistener);
        }
    }

    @Override
    public void destroyFragment() {
        if(enrolmentView != null){
            enrolmentView.destroyFragment();
        }
    }
}
