package com.a3lineng.softwaredev.freedom_app.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {

    private static final String TAG = "SERVICE TAG";

    public MyService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int started) {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < 10; i++)
                            Log.i(TAG, "Generic Service is running");
                    }
                }
        ).start();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}
