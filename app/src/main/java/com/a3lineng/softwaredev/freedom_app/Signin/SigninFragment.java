package com.a3lineng.softwaredev.freedom_app.Signin;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.a3lineng.softwaredev.freedom_app.AssetApplication;
import com.a3lineng.softwaredev.freedom_app.Dashboard.DashboardActivity;
import com.a3lineng.softwaredev.freedom_app.R;
import com.a3lineng.softwaredev.freedom_app.Signup.SignUpActivity;
import com.a3lineng.softwaredev.freedom_app.utilities.SharedPreferencesHelper;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import im.delight.android.location.SimpleLocation;

/**
 * A simple {@link Fragment} subclass.
 */
public class SigninFragment extends android.support.v4.app.Fragment implements SigninView {


    @Inject
    SimpleLocation location;

    EditText editTextusername, editTextPassword;
    Button buttonLogin;
    ProgressBar progressBar;
    SigninPresenter presenter;

    Unbinder unbinder;
    TextView textViewopen_new_user;
    private WeakReference<Context> contextWeakReference;
    private WeakReference<Activity> activityWeakReference;

    public SigninFragment() {
        // Required empty public constructor
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_signin, container, false);
        contextWeakReference = new WeakReference<>(getContext());
        activityWeakReference = new WeakReference<>(getActivity());
        presenter = new SigninPresenterimpl(this, new SigninInteractorImpl(contextWeakReference));
//        Toolbar toolbar = view.findViewById(R.id.toolbar);
//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        getActivity().setTitle(getString(R.string.app_name));
//        getActivity().setTitleColor(android.R.color.white);
//        setHasOptionsMenu(true);


        initializeWidgets(view);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = editTextusername.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();


                presenter.validateLoginCredentials(username, password, location);
            }
        });


        textViewopen_new_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(contextWeakReference.get(), SignUpActivity.class);
                SigninFragment.this.startActivity(intent);
            }
        });

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    private void initializeWidgets(View view) {
        buttonLogin = view.findViewById(R.id.buttonLogin);
        editTextusername = view.findViewById(R.id.editTextloginusername);
        editTextPassword = view.findViewById(R.id.editTextloginpassword);
        textViewopen_new_user = view.findViewById(R.id.open_new_user_text);
        progressBar = view.findViewById(R.id.progBar);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void navigateToHomeActivity() {
        Intent intent = new Intent(contextWeakReference.get(), DashboardActivity.class);
        SharedPreferencesHelper.saveBooleanValue(contextWeakReference.get(), SharedPreferencesHelper.PREF, "is_agent_setup", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    public void showProgress(String message) {
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        activityWeakReference.get().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void alert(String title, String message) {
        Utility.alert(contextWeakReference.get(), title, message, true);
    }

    @Override
    public void showToast(String message) {
        Utility.showToast(contextWeakReference.get(), message);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        activityWeakReference.get().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void updateProgress(String message) {

    }
}