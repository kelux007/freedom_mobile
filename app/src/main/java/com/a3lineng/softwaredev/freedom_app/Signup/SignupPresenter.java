package com.a3lineng.softwaredev.freedom_app.Signup;

import com.a3lineng.softwaredev.freedom_app.base_interactors.BasePresenter;

public interface SignupPresenter extends BasePresenter{
    void validateSignupCredentials(String username, String authData, String newAuthData, String password, String confirmpassword);
}
