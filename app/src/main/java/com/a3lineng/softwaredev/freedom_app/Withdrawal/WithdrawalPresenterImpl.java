package com.a3lineng.softwaredev.freedom_app.Withdrawal;

import android.content.DialogInterface;

import im.delight.android.location.SimpleLocation;

public class WithdrawalPresenterImpl implements WithdrawalPresenter, WithdrawalInteractor.OnWithdrawalFinished{
    private WithdrawalView withdrawalView;
    private WithdrawalInteractor withdrawalInteractor;

    public WithdrawalPresenterImpl(WithdrawalView withdrawalView, WithdrawalInteractor withdrawalInteractor) {
        this.withdrawalView = withdrawalView;
        this.withdrawalInteractor = withdrawalInteractor;
    }

    @Override
    public void withdraw(String accountNumber, String amount, String customerPin, String agentPin) {
        if(withdrawalView != null){
            withdrawalInteractor.withdraw(accountNumber, amount, customerPin, agentPin, this);
        }
    }

    @Override
    public void withdraw2(String amount, String customerPin, String agentPin) {

    }

    @Override
    public void validateWithdrawField(String bankName, String otp, String amount, String customerPin, String agentPin, SimpleLocation location) {

    }

    @Override
    public void validateWithdrawField2(String otp, String amount, String customerPin, String agentPin, SimpleLocation location) {

    }

    @Override
    public void verifyName(String accountName, SimpleLocation location) {

    }

    @Override
    public void requestOtp(String accountNumber, String agentPin, SimpleLocation location) {

    }

    @Override
    public void onDestroy() {
        if(withdrawalView != null){
            withdrawalView = null;
            withdrawalInteractor = null;
        }
    }

    @Override
    public void verifyBenName(String bankCode, String trim, SimpleLocation location) {

    }

    @Override
    public void onError(String title, String message) {
        if(withdrawalView != null){
            withdrawalView.hideProgress();
            withdrawalView.alert(title, message);
        }
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onUpdateProgressMessage(String message) {

    }

    @Override
    public void onStart(String message) {
        if(withdrawalView != null){
            withdrawalView.showProgress(message);
        }
    }

    @Override
    public void onSuccess(String title, String message, DialogInterface.OnClickListener yesListener) {
        if(withdrawalView != null){
            withdrawalView.hideProgress();
            withdrawalView.alert2(title, message, yesListener);
        }
    }
}
