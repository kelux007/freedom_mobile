package com.a3lineng.softwaredev.freedom_app.Signin;

import com.a3lineng.softwaredev.freedom_app.base_interactors.BasePresenter;

import im.delight.android.location.SimpleLocation;

public interface SigninPresenter extends BasePresenter{
    void validateLoginCredentials(String username, String password, SimpleLocation location);
}
