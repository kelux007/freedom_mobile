package com.a3lineng.softwaredev.freedom_app.utilities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.a3lineng.softwaredev.freedom_app.R;
import com.a3lineng.softwaredev.freedom_app.Withdrawal.WithdrawalFragment;
import com.a3lineng.softwaredev.freedom_app.httphelper.HttpHelper;
import com.a3lineng.softwaredev.freedom_app.model.ExportBeneficiary;
import com.a3lineng.softwaredev.freedom_app.model.Transaction;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import static android.support.v4.app.ActivityCompat.requestPermissions;
import static android.support.v4.content.PermissionChecker.checkSelfPermission;

/**
 * Created by OluwatosinA on 21-Mar-18.
 */

public class Utility {
    static Gson json = new Gson();
    private static TelephonyManager telephonyManager;
    @Inject
    HttpHelper httpHelper;
    @Inject
    TerminalDetails terminalDetails;


    public static void verifyPin(Context context, String pin, String accType) {
        boolean correctPinEntered = false;
    }

    public static void showDialog(final Context context, final String status, final String message, boolean... calledInNonUithread) {
        if (calledInNonUithread.length > 0 && calledInNonUithread[0]) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Utility.alert(context, status, message);
                }
            });
            return;
        }
        Utility.alert(context, status, message);
    }

    public static void alert(final Context context, final String title, final String message, boolean... invokedFromNonUIthread) {
        if (invokedFromNonUIthread.length > 0 && invokedFromNonUIthread[0] == true) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    alert(context, title, message);
                }
            });

            return;
        }
        AlertDialog.Builder alertdialogbuilder = new AlertDialog.Builder(context);
        alertdialogbuilder.setTitle(title);
        alertdialogbuilder.setMessage(message);
        alertdialogbuilder.setPositiveButton("Continue", null);
        /*
         * alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int whichButton) {
     		//Do something here where "ok" clicked
    																	 }
																				}
						          );
			alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int whichButton) {
    		//So sth here when "cancel" clicked.
    																     }
																					}
									);
        */
        alertdialogbuilder.setIcon(R.drawable.freedom_icon);
        alertdialogbuilder.create().show();
    }


    public static String stringify(Exception e) {
        StringWriter writer = new StringWriter();
        e.printStackTrace(new PrintWriter(writer));
        return writer.toString();
    }

//    public static boolean isCardPresent(Context context) {
//        return ChipCardReaderUtility.checkCard(context);
//    }

    public static void showProgress(final Context context, final ProgressDialog progress, final String message, final boolean modal, boolean... invokedFromNonUIthread) {

        if (invokedFromNonUIthread.length > 0 && invokedFromNonUIthread[0] == true) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showProgress(context, progress, message, modal);
                }
            });
            return;
        }
        progress.setMessage(message);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        if (modal) {
            progress.setCancelable(false);
            progress.setCanceledOnTouchOutside(false);
        }
        progress.show();

    }

    public static void showProgressBar(final Context context, final ProgressBar progress, final boolean modal, boolean... invokedFromNonUIthread) {

        if (invokedFromNonUIthread.length > 0 && invokedFromNonUIthread[0]) {
            ((Activity) context).runOnUiThread(() -> showProgressBar(context, progress, modal));
            return;
        }
        progress.setIndeterminate(true);
        progress.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        progress.setVisibility(View.VISIBLE);
    }

    public static String parseAmout(String amount) throws NumberFormatException {
        String formattedString = "";
        try {
            Double value = Double.parseDouble(amount);
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.applyPattern("#,###,###,###.##");
            formattedString = formatter.format(value);
        } catch (Exception e) {
            Utility.stringify(e);
            Log.e("Note", e.getMessage());
        }

        return "\u20a6" + formattedString;
    }

    public static String randomNumber(int len) {
        final String AB = "0123456789";
        java.util.Random rnd = new java.util.Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }

    public static String getFormattedDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);

        int mon = month + 1;
        String _month = "";
        if (mon < 10) {
            _month = "0" + mon;
        }

        year = year - 2000;
        return "" + year + "" + _month;
    }

    public static String getFormattedServiceCode(String serviceCode) {
        return serviceCode.substring(15, 18);
    }

    public static String getAccountTypeCode(String accountType) {
        String code = "";
        if (accountType != null) {
            switch (accountType) {
                case "Savings":
                    code = "10";
                    break;
                case "Current":
                    code = "20";
                    break;
                case "Credit":
                    code = "30";
                    break;
                default:
                    code = "10";
            }
        } else {
            return "10";
        }

        return code;
    }

    public static String getAccountType(String accountTypeCode) {
        String code = "";
        switch (accountTypeCode) {
            case "30":
                code = "Credit";
                break;
            case "10":
                code = "Savings";
                break;
            case "20":
                code = "Current";
                break;
        }
        return code;
    }

    public static byte[] sha512Byte(String args) {

        try {
            // Create MessageDigest instance for MD5
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("SHA-512");
            //Add password bytes to digest
            md.update(args.getBytes("UTF-8"));
            //Get the hash's bytes
            return md.digest();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static void showToast(final Context context, final String message) {
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static String encodeBase64(String message) {
        return Base64.encodeToString(Utility.sha512Byte(message), Base64.DEFAULT);
    }


    public static String currentDate() {
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String date = simpleDateformat.format(new Date());
        return date.substring(5).replaceAll("-", "").replaceAll(":", "").replaceAll(" ", "");
    }


    public static boolean validateAccountNumberLength(String accountNumber) {

        return accountNumber.trim().length() == 10;
    }

    public static boolean sdCardAvailableForSaving() {
        String sdCardState = Environment.getExternalStorageState();
        return sdCardState.equals(Environment.MEDIA_MOUNTED) && !sdCardState.equals(Environment.MEDIA_MOUNTED_READ_ONLY);
    }


    public static void hideProgress(final ProgressDialog progress, final Context context, boolean... invokedFromNonUIthread) {

        if (invokedFromNonUIthread.length > 0 && invokedFromNonUIthread[0] == true) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideProgress(progress, context);
                }
            });
            return;
        }
        progress.dismiss();
    }

    public static void setMessage(Context context, final ProgressDialog progress, final String message, boolean... invokedFromNonUIthread) {


        if (invokedFromNonUIthread.length > 0 && invokedFromNonUIthread[0] == true) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progress.setMessage(message);
                }
            });
            return;
        }

        progress.setMessage(message);


    }

//    public static String getExtraTransactionRequestParams() {
//        return String.format("&lat=%s&lon=%s&initiator=%s&beneficiary=%s", LocationUtility.lat, LocationUtility.lon, TerminalDetails.initiator, "");
//        //return String.format("&lat=%s&lon=%s&initiator=%s&beneficiary=%s", "6.4321", "3.2134", TerminalDetails.initiator, "");
//    }

    public static boolean isLastTokenExpired() {
        return (System.currentTimeMillis() - TerminalDetails.lastTokenRequestTime) >= 3600000;
    }

    public static Map<String, String> bankMap(int stringArrayResourceId, Context context) {
        String[] stringArray = context.getResources().getStringArray(stringArrayResourceId);
        Map<String, String> bankMap = new LinkedHashMap<String, String>();
        for (String entry : stringArray) {
            String[] splitResult = entry.split("\\|", 2);
            bankMap.put(splitResult[1], splitResult[0]);
        }
        return bankMap;
    }

    public static String getToken(Context context, boolean isValid) {
        if (isValid) {
            if (TerminalDetails.token != null) {
                if (Utility.isLastTokenExpired()) {
                    HttpHelper.testOauthToken(context);
                }
            } else {
                HttpHelper.testOauthToken(context);
            }
        } else {
            HttpHelper.testOauthToken(context);
        }
        return TerminalDetails.token;
    }

    public static String getDeviceId(Context context) {
        telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, Contract.PERMISSION_READ_PHONE_STATE);
        }
        return telephonyManager.getDeviceId();
    }

    public static void showCameraDialog(final Context context, final String title,
                                        final String message, final DialogInterface.OnClickListener yeslistener, final DialogInterface.OnClickListener nolistener, boolean... invokedFromNonUIthread) {
        if (invokedFromNonUIthread.length > 0 && invokedFromNonUIthread[0] == true) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showCameraDialog(context, title, message, yeslistener, nolistener);
                }
            });
            return;
        }
        AlertDialog.Builder alertdialogbuilder = new AlertDialog.Builder(context);
        alertdialogbuilder.setTitle(title);
        alertdialogbuilder.setMessage(message);
        alertdialogbuilder.setPositiveButton("OPEN CAMERA", yeslistener);
        alertdialogbuilder.setNegativeButton("SELECT PICTURE", nolistener);

        alertdialogbuilder.setIcon(R.drawable.freedom_icon);
        alertdialogbuilder.create().show();
    }

    public static boolean hasBalance(String walletBalance) {
        return Double.parseDouble(walletBalance) > 0.0;
    }

    public static Bitmap getBitmap(String careGiverPicture) {
        byte[] image = Base64.decode(careGiverPicture, Base64.NO_WRAP);
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

//    public static void showbenficiaryDetailsDialog(final Context context, final View view, final String title,
//                                                   final String name, final String image, boolean... invokedFromNonUIthread) {
//        if (invokedFromNonUIthread.length > 0 && invokedFromNonUIthread[0] == true) {
//            ((Activity) context).runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    showbenficiaryDetailsDialog(context, view, title, name, image);
//                }
//            });
//
//            return;
//        }
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setTitle(title);
//        final TextView beneficiaryName = view.findViewById(R.id.beneficiaryName);
//        final ImageView beneficiaryImage = view.findViewById(R.id.largeProfileImage);
//        beneficiaryImage.setImageBitmap(Utility.getBitmap(image));
//        beneficiaryName.setText(name);
//        builder.setView(view);
//        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//
//            }
//        });
//
//        builder.show();
//
//    }

    public static String getBase64FromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);

        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static JSONArray generateBeneficiaryDetails(List<ExportBeneficiary> beneficiaries) {
        Gson gson = new Gson();
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(gson.toJson(beneficiaries));
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return jsonArray;
    }

    public static JSONArray generateTransactionsDetails(List<Transaction> transactions) {
        Gson gson = new Gson();
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(gson.toJson(transactions));
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return jsonArray;
    }

//    public static Bitmap textToImage(String text, int width, int height) throws WriterException, NullPointerException {
//        BitMatrix bitMatrix;
//        try {
//            bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.DATA_MATRIX.QR_CODE,
//                    width, height, null);
//        } catch (IllegalArgumentException Illegalargumentexception) {
//            return null;
//        }
//
//        int bitMatrixWidth = bitMatrix.getWidth();
//        int bitMatrixHeight = bitMatrix.getHeight();
//        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];
//
//        int colorWhite = 0xFFFFFFFF;
//        int colorBlack = 0xFF000000;
//
//        for (int y = 0; y < bitMatrixHeight; y++) {
//            int offset = y * bitMatrixWidth;
//            for (int x = 0; x < bitMatrixWidth; x++) {
//                pixels[offset + x] = bitMatrix.get(x, y) ? colorBlack : colorWhite;
//            }
//        }
//        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);
//
//        bitmap.setPixels(pixels, 0, width, 0, 0, bitMatrixWidth, bitMatrixHeight);
//        return bitmap;
//    }

    public static void printAlert(final Context context, final String title, final String message, final DialogInterface.OnClickListener listener, boolean... invokedFromNonUIthread) {
        if (invokedFromNonUIthread.length > 0 && invokedFromNonUIthread[0] == true) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    printAlert(context, title, message, listener);
                }
            });

            return;
        }
        AlertDialog.Builder alertdialogbuilder = new AlertDialog.Builder(context);
        alertdialogbuilder.setTitle(title);
        alertdialogbuilder.setMessage(message);
        alertdialogbuilder.setPositiveButton("Continue", listener);
        alertdialogbuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }
        );

        alertdialogbuilder.setIcon(R.drawable.freedom_icon);
        alertdialogbuilder.create().show();
    }

    public static void alert2(final Context context, final String title, final String message, final DialogInterface.OnClickListener listener, boolean... invokedFromNonUIthread) {
        if (invokedFromNonUIthread.length > 0 && invokedFromNonUIthread[0] == true) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    alert2(context, title, message, listener);
                }
            });

            return;
        }
        AlertDialog.Builder alertdialogbuilder = new AlertDialog.Builder(context);
        alertdialogbuilder.setTitle(title);
        alertdialogbuilder.setMessage(message);
        alertdialogbuilder.setPositiveButton("Continue", listener);


        alertdialogbuilder.setIcon(R.drawable.freedom_icon);
        alertdialogbuilder.create().show();
    }

    public static void showInputOTPDialog(final Context context, final int host, final View view, final String title,
                                          final String message, boolean... invokedFromNonUIthread) {
        if (invokedFromNonUIthread.length > 0 && invokedFromNonUIthread[0]) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showInputOTPDialog(context, host, view, title, message);
                }
            });

            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        final EditText input = view.findViewById(R.id.editText_otp);
        input.setInputType(InputType.TYPE_NULL);
        input.setTransformationMethod(new AsterikPasswordTransformationMethod());
        final AlertDialog dialog = builder.create();
        final Button btnProceed = view.findViewById(R.id.ottp_proceed);
        btnProceed.setOnClickListener(v -> {
            switch (host) {
                case Contract.CASH_WITHDRAWAL:
                    dialog.dismiss();
                    WithdrawalFragment.callback(input.getText().toString().trim());
                    break;
//                case Contract.FUNDS_TRANSFER:
//                    dialog.dismiss();
//                    TransferFragment.callback(input.getText().toString().trim());
//                    break;
            }
        });
        final TextView tvMessage = view.findViewById(R.id.tvTitle);
        tvMessage.setText(message);
        dialog.setView(view);
//        builder.setPositiveButton("OK", (dialog, which) -> {
//            dialog.cancel();
//            switch (host) {
//                case Contract.CASH_WITHDRAWAL:
//                    Withdrawal.callback(input.getText().toString().trim());
//                    break;
//            }
//        });
        //builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel());
//        AlertDialog alertDialog = builder.create();
//        alertDialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}




