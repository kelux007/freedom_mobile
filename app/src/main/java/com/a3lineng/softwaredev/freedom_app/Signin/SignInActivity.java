package com.a3lineng.softwaredev.freedom_app.Signin;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.a3lineng.softwaredev.freedom_app.R;

public class SignInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        if (savedInstanceState != null) {
            getFragmentManager().executePendingTransactions();
            android.app.Fragment fragmentById = getFragmentManager().findFragmentById(R.id.fragment_container);


            if (fragmentById != null) {
                getFragmentManager().beginTransaction().remove(fragmentById).commit();
            }
        }

        SigninFragment signinFragment = new SigninFragment();
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.fragment_container, signinFragment, "login").commit();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
