package com.a3lineng.softwaredev.freedom_app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class BankingApplication extends Application {
    private static BankingApplication bankingApplication;

    private SharedPreferences pref;


    public static Context getAppContext() {
        return bankingApplication.getApplicationContext();
    }

    public void onCreate() {
        super.onCreate();

        bankingApplication = this;

    }

}
