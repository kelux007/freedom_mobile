package com.a3lineng.softwaredev.freedom_app.Signup;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.a3lineng.softwaredev.freedom_app.AssetApplication;
import com.a3lineng.softwaredev.freedom_app.R;
import com.a3lineng.softwaredev.freedom_app.Signin.SignInActivity;
import com.a3lineng.softwaredev.freedom_app.utilities.SharedPreferencesHelper;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import im.delight.android.location.SimpleLocation;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignupFragment extends android.support.v4.app.Fragment implements SignUpView {
    EditText etUsername, etPin, etNewPin, etPassword, etConfirmPassword;
    String strUsername, strPin, strNewPin, strPassword, strConfirmPassword;
    Button btnCreateAgent;
    ProgressBar progressBar;
    WeakReference<Context> contextWeakReference;
    WeakReference<Activity> activityWeakReference;
    @Inject
    SimpleLocation location;
    SignupPresenter presenter;

    public SignupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        AssetApplication.getAssetApplication().getLocationComponent().inject(this);
        contextWeakReference = new WeakReference<>(getContext());
        activityWeakReference = new WeakReference<>(getActivity());
        presenter = new SignupPresenterImpl(this, new SignUpInteractorImpl(contextWeakReference));
        initializeWidgets(view);

        btnCreateAgent.setOnClickListener(v -> {
            strUsername = etUsername.getText().toString().trim();
            strPin = etPin.getText().toString().trim();
            strNewPin = etNewPin.getText().toString().trim();
            strPassword = etPassword.getText().toString().trim();
            strConfirmPassword = etConfirmPassword.getText().toString().trim();

            presenter.validateSignupCredentials(strUsername, strPin, strNewPin, strPassword, strConfirmPassword);
        });

        return view;
    }

    private void initializeWidgets(View view) {
        etUsername = view.findViewById(R.id.editText_username);
        etPin = view.findViewById(R.id.editTextPin);
        etNewPin = view.findViewById(R.id.editTextnewPin);
        etPassword = view.findViewById(R.id.editTextPassword);
        etConfirmPassword = view.findViewById(R.id.editTextConfirmpassword);
        btnCreateAgent = view.findViewById(R.id.buttonCreateAgent);

        progressBar = view.findViewById(R.id.progBar);
    }

    @Override
    public void navigateToLogin() {
        Intent intent = new Intent(contextWeakReference.get(), SignInActivity.class);
        SharedPreferencesHelper.saveBooleanValue(contextWeakReference.get(), SharedPreferencesHelper.PREF, "is_agent_setup", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    public void showProgress(String message) {
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        activityWeakReference.get().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void alert(String title, String message) {
        Utility.alert(contextWeakReference.get(), title, message, true);
    }

    @Override
    public void showToast(String message) {
        Utility.showToast(contextWeakReference.get(), message);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        activityWeakReference.get().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void updateProgress(String message) {

    }
}
