package com.a3lineng.softwaredev.freedom_app.enrolment;

import com.a3lineng.softwaredev.freedom_app.base_interactors.BasePresenter;

public interface EnrolmentPresenter extends BasePresenter {
    void validateEnrolmentField(String name, String bvn, String motherMaidenName, String dob, String phone);
}
