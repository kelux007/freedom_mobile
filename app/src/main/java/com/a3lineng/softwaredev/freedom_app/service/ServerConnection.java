package com.a3lineng.softwaredev.freedom_app.service;

import android.os.Handler;
import android.os.Message;

import com.a3lineng.softwaredev.freedom_app.utilities.Contract;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

/**
 * Created by OluwatosinA on 24-Mar-18.
 */

public class ServerConnection {
    private WebSocket mWebSocket;
    private OkHttpClient mClient;
    private String mServerUrl;
    private Handler mMessageHandler;
    private Handler mStatusHandler;
    private ServerListener mListener;

    public ServerConnection(String url) {
        mClient = new OkHttpClient.Builder()
                .readTimeout(5, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();

        mServerUrl = url;
    }

    public void connect(ServerListener listener) {
        Request request = new Request.Builder()
                .url(mServerUrl)
                .build();
        mWebSocket = mClient.newWebSocket(request, new SocketListener());
        mWebSocket.send("Hello, Testing Connection!");
        mListener = listener;
        mMessageHandler = new Handler(msg -> {
            mListener.onNewMessage((String) msg.obj);
            return true;
        });
        mStatusHandler = new Handler(msg -> {
            mListener.onStatusChange((ConnectionStatus) msg.obj);
            return true;
        });
    }

    public void disconnect() {
        mWebSocket.close(1000, "Completed");
        mListener = null;
        mMessageHandler.removeCallbacksAndMessages(null);
        mStatusHandler.removeCallbacksAndMessages(null);
    }

    public void sendMessage(String message) {
        mWebSocket.send(message);
    }

    public enum ConnectionStatus {
        DISCONNECTED,
        CONNECTED
    }

    public interface ServerListener {
        void onNewMessage(String message);

        void onStatusChange(ConnectionStatus status);
    }

    public class SocketListener extends WebSocketListener {

        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            super.onOpen(webSocket, response);

            try {
                System.out.println("On Open: Observe Response: " + response.body().string());
            } catch (Exception e) {
                Utility.stringify(e);
            }

            Message message = mMessageHandler.obtainMessage(Contract.ACTIVATE_POS_MESSAGE, ConnectionStatus.CONNECTED);
            mStatusHandler.sendMessage(message);
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            super.onMessage(webSocket, text);
            System.out.println("OnMessage: Observe result: " + text);
            Message message = mMessageHandler.obtainMessage(Contract.ACTIVATE_POS_MESSAGE, text);
            mMessageHandler.sendMessage(message);
        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
            super.onMessage(webSocket, bytes.toString());
            System.out.println("OnMessage: Observe result: " + bytes.toString());
            Message message = mMessageHandler.obtainMessage(Contract.ACTIVATE_POS_MESSAGE, bytes.toString());
            mMessageHandler.sendMessage(message);
        }


        @Override
        public void onClosed(WebSocket webSocket, int code, String reason) {
            super.onClosed(webSocket, code, reason);
            System.out.println("OnClosed: Observe Reason: " + reason);
            Message message = mMessageHandler.obtainMessage(Contract.ACTIVATE_POS_MESSAGE, ConnectionStatus.DISCONNECTED);
            mMessageHandler.sendMessage(message);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            super.onFailure(webSocket, t, response);
            System.out.println("OnFailure: Observe Response: " + response);
            disconnect();
        }
    }
}