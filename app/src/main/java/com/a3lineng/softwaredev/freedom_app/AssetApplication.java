package com.a3lineng.softwaredev.freedom_app;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.a3lineng.softwaredev.freedom_app.component.DaggerLocationComponent;
import com.a3lineng.softwaredev.freedom_app.component.LocationComponent;
import com.a3lineng.softwaredev.freedom_app.modules.LocationModule;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class AssetApplication extends Application {
    private static AssetApplication assetApplication;
    private LocationComponent locationComponent;

    public static Context getAppContext() {
        return assetApplication.getApplicationContext();
    }

    public void onCreate() {
        super.onCreate();

        assetApplication = this;

        locationComponent = DaggerLocationComponent.builder()
                .locationModule(new LocationModule(assetApplication))
                .build();

    }

    public LocationComponent getLocationComponent() {
        return locationComponent;
    }

    public static AssetApplication getAssetApplication() {
        return assetApplication;
    }

    public static final String TAG = AssetApplication.class
            .getSimpleName();

    private RequestQueue mRequestQueue;

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
