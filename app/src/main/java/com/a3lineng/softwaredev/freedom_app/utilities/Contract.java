package com.a3lineng.softwaredev.freedom_app.utilities;

/**
 * Created by OluwatosinA on 21-Mar-18.
 */

public class Contract {

    public static final int PERMISSION_READ_PHONE_STATE = 100;
    public static final int PERMISSION_READ_PERMISSION = 101;
    public static final int POS_FUNDS_TRANSFER = 50;
    public static final int FUNDS_TRANSFER = 50;
    public static final int CASH_WITHDRAWAL = 60;
    public static final int POS_CASH_WITHDRAWAL = 60;
    public static final int POS_CASH_DEPOSIT = 70;
    public static final int UPDATE_INTERVAL_SECONDS = 2 * 60 * 60;
    public static final long TIME_BTW_LOCATION_UPDATES = 60 * 1000; //millseconds i.e 1 Minutes
    public static final long DISTANCES_BTW_LOCATION_UPDATES = 10;
    public static final String FORCED_UPDATE_ID = "3";
    public static final int ALL_PERMISSIONS_REQUEST = 25;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 400;
    public static final int ACTIVATE_POS_MESSAGE = 20;
    public static final int MY_PERMISSIONS_REQUEST_DEVICE = 600;
    public static final String DOWNLOAD_RESP_ACTION = "com.a3line.softwaredev.nipostmobile.intent.action.DOWNLOAD_PROCESSED";
    public static final String DOWNLOAD_FINISHED_ACTION = "com.a3line.softwaredev.nipostmobile.intent.action.DOWNLOAD_FINISHED";
    public static final String DOWNLOAD_STATUS = "download_out_msg";
    public static final String SEEK_SERVER = "MESSAGE.SEEK";
    public static final String SEEN_SERVER = "MESSAGE.SEEN";
    public static final String ERROR_ACC_NUMBER_LENGTH = "accountNumberLength";
    public static final String ERROR_ACC_ENQUIRY = "accountEnquiry";
    public static final String ERROR_FUNDS_TRANSFER = "fundsTransfer";
    public static final String ERROR_TOKEN = "tokenError";
    public static final String ERROR_DEPOSIT = "depositError";
    public static String terminalCurrencyCode = "0566";
    public static String terminalCountryCode = "0566";
    public int WITHDRAWAL = 120;

    public enum TransactionCodes {
        WITHDRAWAL,
        DEPOSIT,
        TRANSFER,
        BALANCE_ENQUIRY
    }
}
