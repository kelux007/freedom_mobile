package com.a3lineng.softwaredev.freedom_app.model;

public class Bank {
    private Long id;
    private String bankName;
    private String magtiponCode;
    private String opayCode;
    private String cbnCode;

    public Bank(String bankName, String magtiponCode, String opayCode, String cbnCode) {
        this.id = id;
        this.bankName = bankName;
        this.magtiponCode = magtiponCode;
        this.opayCode = opayCode;
        this.cbnCode = cbnCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getMagtiponCode() {
        return magtiponCode;
    }

    public void setMagtiponCode(String magtiponCode) {
        this.magtiponCode = magtiponCode;
    }

    public String getOpayCode() {
        return opayCode;
    }

    public void setOpayCode(String opayCode) {
        this.opayCode = opayCode;
    }

    public String getCbnCode() {
        return cbnCode;
    }

    public void setCbnCode(String cbnCode) {
        this.cbnCode = cbnCode;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "id=" + id +
                ", bankName='" + bankName + '\'' +
                ", magtiponCode='" + magtiponCode + '\'' +
                ", opayCode='" + opayCode + '\'' +
                ", cbnCode='" + cbnCode + '\'' +
                '}';
    }
}
