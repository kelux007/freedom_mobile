package com.a3lineng.softwaredev.freedom_app.base_interactors;

public interface BaseFinishedListener {
    void onError(String title, String message);

    void onSuccess();

    void onUpdateProgressMessage(String message);

    void onStart(String message);
}
