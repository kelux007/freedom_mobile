package com.a3lineng.softwaredev.freedom_app.utilities;


import android.app.Activity;
import android.content.Context;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;


public class PropertyResource {

    Properties prop = new Properties();
    InputStream input = null;
    private Context context;

    public PropertyResource(Context context) {
        this.context = context;
    }

    public String getV(String name, String file) {

        try {

            //input = getClass().getClassLoader().getResourceAsStream(file); //for non android

            input = ((Activity) context).getBaseContext().getAssets().open(file); //for android

            // load a properties file
            prop.load(input);


        } catch (NullPointerException ex) {
            System.out.printf("Null pointer exception\n");
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            System.out.printf("File name %s was not found\n", file);
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.printf("File name %s caused IO exception\n", file);
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        String value = prop.getProperty(name);
        return (value == null ? "" : value);
    }


    public boolean setV(String name, String value, String fileename) {

        Properties prop = new Properties();
        OutputStream output = null;

        try {

            input = getClass().getResourceAsStream(fileename); //search the file in same location as the class


            // load file
            prop.load(input);
            //put value in loaded file
            prop.put(name, value);


            //put the loaded  and altered file in write stream
            output = new FileOutputStream(new File(getClass().getResource(fileename).toURI()).getAbsolutePath());


            // save properties
            prop.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
            return false;
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return true;
    }


    public ArrayList<String[]> getAll(String file) {

        ArrayList<String[]> all = new ArrayList<String[]>();
        try {

            input = getClass().getResourceAsStream(file);


            // load a properties file
            prop.load(input);

            Enumeration<?> e = prop.propertyNames();
            while (e.hasMoreElements()) {
                String key = (String) e.nextElement();
                String value = prop.getProperty(key);
                all.add(new String[]{key, value});
                System.out.println("Key : " + key + ", Value : " + value);

            }

        } catch (NullPointerException ex) {
            System.out.printf("Null pointer exception\n");
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            System.out.printf("File name %s was not found\n", file);
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.printf("File name %s caused IO exception\n", file);
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        return all;
    }

	 

	
	 
	/*public static void main(String[]arg){
		System.out.println(PropertyResource.class.getResource("."));
		System.out.println(java.util.Arrays.deepToString(new PropertyResource().getAll("modularunit.properties").toArray()));
		
	}*/

}

