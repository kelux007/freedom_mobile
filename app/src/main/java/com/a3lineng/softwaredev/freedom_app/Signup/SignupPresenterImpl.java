package com.a3lineng.softwaredev.freedom_app.Signup;

public class SignupPresenterImpl implements SignupPresenter, SignUpInteractor.OnSignupFinishedListener {
    private SignUpView signUpView;
    private SignUpInteractor signUpInteractor;

    SignupPresenterImpl(SignUpView signUpView, SignUpInteractor signUpInteractor) {
        this.signUpView = signUpView;
        this.signUpInteractor = signUpInteractor;
    }

    @Override
    public void validateSignupCredentials(String username, String authData, String newAuthData, String password, String confirmpassword) {
        if (signUpView != null) {
            signUpInteractor.signupAgent(username, authData, newAuthData, password, confirmpassword, this);
        }
    }

    @Override
    public void onError(String title, String message) {
        if (signUpView != null) {
            signUpView.hideProgress();
            signUpView.alert(title, message);
        }
    }

    @Override
    public void onSuccess() {
        if (signUpView != null) {
            signUpView.navigateToLogin();
        }
    }

    @Override
    public void onUpdateProgressMessage(String message) {

    }

    @Override
    public void onStart(String message) {
        signUpView.showProgress(message);
    }

    @Override
    public void onDestroy() {
        if (signUpView != null) {
            signUpView = null;
        }
    }
}
