/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a3lineng.softwaredev.freedom_app.security;


import com.a3lineng.softwaredev.freedom_app.AssetApplication;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.KeyStore;

import javax.crypto.SecretKey;


public class KeyStoreWrapper {


    public static SecretKey getKey() throws Exception {

        String fileName = "3LGVTAPI.bks";
        String Keyalias = "SecretKeyAlias";
        String fileloadPassword = "com.mooasoft";
        String KeyloadPassword = "3LR@tio12to3Magnum";

        //InputStream in = context.getResources().openRawResource(R.raw.aglite);
        InputStream file = AssetApplication.getAppContext().getAssets().open(fileName);
        //final KeyStore keyStore = KeyStore.getInstance("BKS", "BC");
        final KeyStore keyStore = KeyStore.getInstance("BKS", "BC");


        if (file != null) {
            // .keystore file already exists => load it
            keyStore.load(file, fileloadPassword.toCharArray());
            // keyStore.load(new FileInputStream(new File));
            System.out.println("Existing .keystore file loaded!");
        } else {
            // .keystore file not created yet => create it
            throw new FileNotFoundException("File could not be located");
        }

        SecretKey keyFound = (SecretKey) keyStore.getKey(Keyalias, KeyloadPassword.toCharArray());
        return keyFound;
    }
}
