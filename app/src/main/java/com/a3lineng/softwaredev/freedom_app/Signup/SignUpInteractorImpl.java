package com.a3lineng.softwaredev.freedom_app.Signup;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.a3lineng.softwaredev.freedom_app.httphelper.Backend;
import com.a3lineng.softwaredev.freedom_app.httphelper.Token;
import com.a3lineng.softwaredev.freedom_app.security.SecretKeyCrypt;
import com.a3lineng.softwaredev.freedom_app.utilities.SharedPreferencesHelper;
import com.a3lineng.softwaredev.freedom_app.utilities.TerminalDetails;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class SignUpInteractorImpl implements SignUpInteractor {
    private WeakReference<Context> contextWeakReference;
    private static final String TAG = SignUpInteractorImpl.class.getSimpleName();

    SignUpInteractorImpl(WeakReference<Context> contextWeakReference) {
        this.contextWeakReference = contextWeakReference;
    }

    @Override
    public void signupAgent(String username, String authData, String newAuthData, String password, String confirmpassword, OnSignupFinishedListener listener) {
        if(TextUtils.isEmpty(username) || TextUtils.isEmpty(authData) || TextUtils.isEmpty(newAuthData)|| TextUtils.isEmpty(password) || TextUtils.isEmpty(confirmpassword)){
            listener.onError("Error", "All fields are required");
        }else{
            if(password.equals(confirmpassword)){
                if(Utility.isNetworkConnected(contextWeakReference.get())){
                    new SignupAgent(contextWeakReference, username, newAuthData, authData, password, confirmpassword, listener).execute();
                }else{
                    listener.onError("Error", "Internet connection not available");
                }
            }else{
                listener.onError("Error", "Password does not match");
            }
        }
    }

    static class SignupAgent extends AsyncTask<Void, String, String> {
        private final String username;
        private final String newAuthData;
        private final String authData;
        private final String password;
        private final String confirmpassword;
        private final OnSignupFinishedListener listener;
        private final WeakReference<Context> contextWeakReference;

        SignupAgent(WeakReference<Context> contextWeakReference, String username, String newAuthData, String authData, String password, String confirmpassword, OnSignupFinishedListener listener) {
            this.username = username;
            this.newAuthData = newAuthData;
            this.password = password;
            this.authData = authData;
            this.confirmpassword = confirmpassword;
            this.listener = listener;
            this.contextWeakReference = contextWeakReference;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listener.onStart("Setting up Agent");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null) {
                try {
                    JSONObject jsonObject = new JSONObject(s);

                    String respCode = jsonObject.getString("respCode");
                    String respDescription = jsonObject.getString("respDescription");

                    switch (respCode) {
                        case "00":
                            String agentId = jsonObject.getString("agentId");
                            String clientId = jsonObject.getString("clientId");
                            System.out.println("ClientId: " + jsonObject.getString("clientId"));
                            String secretKey = jsonObject.getString("secretKey");
                            String deviceSetupStatus = jsonObject.getString("deviceSetupStatus");
                            String balance = jsonObject.getString("balnce");

                            TerminalDetails.clientId = jsonObject.getString("clientId");
                            TerminalDetails.shared_secret_key = jsonObject.getString("secretKey");
                            TerminalDetails.agentId = jsonObject.getString("agentId");
                            TerminalDetails.deviceSetupStatus = jsonObject.getString("deviceSetupStatus");
                            TerminalDetails.balance = jsonObject.getString("balnce");

                            SharedPreferencesHelper.saveValue(contextWeakReference.get(), "agent_username", username, SharedPreferencesHelper.PREF);
                            SharedPreferencesHelper.saveValue(contextWeakReference.get(), "agent_password", password, SharedPreferencesHelper.PREF);
                            SharedPreferencesHelper.saveValue(contextWeakReference.get(), "agent_pin", authData, SharedPreferencesHelper.PREF);
                            SharedPreferencesHelper.saveValue(contextWeakReference.get(), "clientId", clientId, SharedPreferencesHelper.PREF);
                            SharedPreferencesHelper.saveValue(contextWeakReference.get(), "shared_secret_key", secretKey, SharedPreferencesHelper.PREF);
                            SharedPreferencesHelper.saveValue(contextWeakReference.get(), "agentId", agentId, SharedPreferencesHelper.PREF);
                            SharedPreferencesHelper.saveValue(contextWeakReference.get(), "deviceSetupStatus", deviceSetupStatus, SharedPreferencesHelper.PREF);
                            SharedPreferencesHelper.saveValue(contextWeakReference.get(), "balance", balance, SharedPreferencesHelper.PREF);

                            SharedPreferencesHelper.saveBooleanValue(contextWeakReference.get(), SharedPreferencesHelper.PREF, "is_agent_setup", true);

                            listener.onSuccess();
                            break;
                        case "109":
                            new SignupAgent(contextWeakReference, username, newAuthData, authData, password, confirmpassword, listener).execute();
                            break;
                        default:
                            listener.onError("Error", respDescription);
                            break;
                    }

                } catch (JSONException e) {
                    Utility.stringify(e);
                } catch (Exception ex) {
                    Utility.stringify(ex);
                    Log.e(TAG, ex.getMessage());
                }
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected String doInBackground(Void... voids) {
            String response = "";
            try {
                if(TerminalDetails.isTokenExpired){
                    new Token.GetToken(contextWeakReference).execute();
                }
                response = Backend.agentSetup(contextWeakReference.get(), username, newAuthData, authData, password, confirmpassword);
            } catch (Exception ex) {
                Utility.stringify(ex);
                Log.e(TAG, ex.getMessage());
                listener.onError("Error", "An error occurred, please try again");
            }
            return response;
        }
    }
}
