package com.a3lineng.softwaredev.freedom_app.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class MyBinderService extends Service {
    private final IBinder binder = new MyLocalBinder();

    @Override
    public IBinder onBind(Intent arg0) {
        return binder;
    }

    public String getName() {
        return "Ny Binder Service returned this";
    }


    public class MyLocalBinder extends Binder {
        public MyBinderService getService() {
            return MyBinderService.this;
        }
    }
}
