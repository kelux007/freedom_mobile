package com.a3lineng.softwaredev.freedom_app.Deposit;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.a3lineng.softwaredev.freedom_app.R;
import com.a3lineng.softwaredev.freedom_app.listeners.OnDestroyFragmentRequest;
import com.a3lineng.softwaredev.freedom_app.utilities.LocationUtility;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import java.lang.ref.WeakReference;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import im.delight.android.location.SimpleLocation;
public class DepositFragment extends Fragment implements DepositView{


    EditText accountnumber, etAmount, etNarration, etCusomerPin, etAgentPin, etAccountName;
    Button proceed;
    private static OnDestroyFragmentRequest mCallback;
    private static WeakReference<Fragment> fragmentWeakReference;
    ProgressDialog progressDialog;
    DepositPresenter presenter;
    WeakReference<Context> contextWeakReference;
    SimpleLocation location;

    Unbinder unbinder;

    public DepositFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_deposit, container, false);
        contextWeakReference = new WeakReference<>(getContext());
        fragmentWeakReference = new WeakReference<Fragment>(this);
        initializeWidgets(view);
        location = new SimpleLocation(contextWeakReference.get());
        if (!location.hasLocationEnabled()) {
            SimpleLocation.openSettings(contextWeakReference.get());
        }

        location.setListener(() -> {
            LocationUtility.lat = String.valueOf(location.getLatitude());
            LocationUtility.lon = String.valueOf(location.getLongitude());
        });

        presenter = new DepositPresenterImpl(new DepositInteractorImpl(contextWeakReference.get()), this);

        proceed.setOnClickListener(v -> {
            String accountNumber = accountnumber.getText().toString().trim();
            String amount = etAmount.getText().toString().trim();
            String agentPin = etAgentPin.getText().toString().trim();
            String narration = etNarration.getText().toString();
            String customerPin = etCusomerPin.getText().toString();


            presenter.validateDepositField(accountNumber, agentPin, amount, narration, customerPin, location);
        });


        accountnumber.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                if (accountnumber.length() >= 10)
                    presenter.validateName(accountnumber.getText().toString().trim(), location);
            }else{
                Toast.makeText(contextWeakReference.get(), "Get Focus", Toast.LENGTH_SHORT).show();
            }
        });

        unbinder = ButterKnife.bind(this, view);
        return view;

    }
    private void initializeWidgets(View view) {
        etAccountName = view.findViewById(R.id.deposit_acctname);
        accountnumber = view.findViewById(R.id.deposit_acctnum);
        etAmount = view.findViewById(R.id.deposit_amount);
        proceed = view.findViewById(R.id.deposit_proceed_button);
        etAgentPin = view.findViewById(R.id.deposit_agentpin);
        etNarration = view.findViewById(R.id.deposit_narration);
        etCusomerPin = view.findViewById(R.id.deposit_customerpin);
        progressDialog = new ProgressDialog(contextWeakReference.get());
    }

    @Override
    public void showProgress(String message) {
        Utility.showProgress(contextWeakReference.get(), progressDialog, message, true, true);
    }

    @Override
    public void hideProgress() {
        Utility.hideProgress(progressDialog, contextWeakReference.get(), true);
    }

    @Override
    public void destroyFragment() {
        destroyFragment(fragmentWeakReference.get());
    }

    @Override
    public void updateAccountName(String accountName) {
        etAccountName.setText(accountName);
        etAccountName.setEnabled(false);
        etAccountName.setVisibility(View.VISIBLE);
    }

    @Override
    public void showAlert(String title, String message) {
        Utility.alert(contextWeakReference.get(), title, message, true);
    }

    @Override
    public void updateProgress(String message) {
        progressDialog.setMessage(message);
    }

    @Override
    public void showAlert2(String title, String message, DialogInterface.OnClickListener yeslistener, DialogInterface.OnClickListener nolistener) {

    }

    @Override
    public void showAlert4(String title, String message, DialogInterface.OnClickListener yeslistener) {

    }

    @Override
    public void showAlert5(String title, String message, DialogInterface.OnClickListener cancel, DialogInterface.OnClickListener retry) {

    }

    @Override
    public void showAlert3(String title, String message, DialogInterface.OnClickListener yeslistener, DialogInterface.OnClickListener nolistener) {

    }

    private static void destroyFragment(Fragment fragment) {
        // Send the event to the host activity
        mCallback.onFragmentDestroy(fragment);
    }

    @Override
    public void onResume() {
        super.onResume();
        location.beginUpdates();
    }



    @Override
    public void onPause() {
        super.onPause();
        location.endUpdates();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnDestroyFragmentRequest) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
}
