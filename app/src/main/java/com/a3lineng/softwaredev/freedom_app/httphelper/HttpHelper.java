package com.a3lineng.softwaredev.freedom_app.httphelper;

import android.content.Context;
import android.util.Base64;

import com.a3lineng.softwaredev.freedom_app.security.SecretKeyCrypt;
import com.a3lineng.softwaredev.freedom_app.utilities.TerminalDetails;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

/**
 * Created by OluwatosinA on 15-Mar-18.
 */

public class HttpHelper {
    public static String token;
    static Gson json = new Gson();
    static String ipport = "localhost:8080/gravity";//"10.4.4.31:9090/gravity";// ""+ipport+"";
    static String client_id = "8f85517967795eeef66c225f7883bdcb";
    static String shared_secret_key = "b0aea0967f3f9e8f255c439b700a6e07";
    static String Username = "2348103946023";
    @Inject
    TerminalDetails terminalDetails;


    public static String get(String url, String data, String method, Map<String, String> headers) throws IOException {
        String response;
        System.out.println("URL: " + url);

        URL _url = new URL(url);
        System.out.println("SENDING TO URL " + url);
        System.out.println("Observe data: " + data);
        // Create the urlConnection
        HttpURLConnection urlConnection = (HttpURLConnection) _url.openConnection();
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);


        if (headers != null)
            for (Map.Entry<String, String> header : headers.entrySet()) {
                urlConnection.setRequestProperty(header.getKey(), header.getValue());
            }
        //urlConnection.setRequestProperty("Content-Type", "application/octet-stream");
        //  urlConnection.setRequestProperty("Content-Type", "text/plain");
        urlConnection.setRequestMethod(method);
        if (data != null) {
            urlConnection.setRequestProperty("Content-Length", Integer.toString(data.length()));
            urlConnection.getOutputStream().write(data.getBytes("UTF8"));
        }
        int statusCode = urlConnection.getResponseCode();

        if (statusCode == 200) {

            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());

            response = convertInputStreamToString(inputStream);
            return response;
            // From here you can convert the string to JSON with whatever JSON parser you like to use

            // After converting the string to JSON, I call my custom callback. You can follow this process too, or you can implement the onPostExecute(Result) method

        } else {
            // Status code is not 200
            // Do something to handle the error
            response = null;
        }


        return response;
    }

//    public static String get(String url, String method) throws IOException {
//        String response;
//        System.out.println("URL: " + url); //http://:/gravity/api/checkUpdates?version=1&forced_id=3
//
//        //for logging on gravity portal
////        url = url + Utility.getExtraTransactionRequestParams();
////        URL _url = new URL(url);url
//
//        System.out.println("SENDING TO URL " + url); //http://:/gravity/api/checkUpdates?version=1&forced_id=3&lat=&lon=&initiator=&beneficiary=
//
//        // Create the urlConnection
//        HttpURLConnection urlConnection = (HttpURLConnection) _url.openConnection();
//        urlConnection.setDoInput(true);
//        urlConnection.setDoOutput(true);
//
//        urlConnection.setRequestProperty("Content-Type", "text/plain");
//        urlConnection.setRequestMethod(method);
//
//        int statusCode = urlConnection.getResponseCode();
//
//        if (statusCode == 200) {
//
//            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
//
//            response = convertInputStreamToString(inputStream);
//            return response;
//            // From here you can convert the string to JSON with whatever JSON parser you like to use
//
//            // After converting the string to JSON, I call my custom callback. You can follow this process too, or you can implement the onPostExecute(Result) method
//
//        } else {
//            // Status code is not 200
//            // Do something to handle the error
//            response = null;
//        }
//        return response;
//    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line).append('\n');
        }
        return total.toString();
    }

    public static void testOauthToken(Context context) {

        try {
            //remember to change back to TerminalDetails.client_id
            //String client_id = "8f85517967795eeef66c225f7883bdcb";
            //String shared_secret_key = "b0aea0967f3f9e8f255c439b700a6e07";
            String body = "grant_type=client_credentials";
            Map headers = new HashMap<String, Object>();
            headers.put("Content-Type", "application/x-www-form-urlencoded");
            headers.put("authorization", "Basic " + Base64.encodeToString((TerminalDetails.client_id + ":" + TerminalDetails.shared_secret_key).getBytes("UTF8"), Base64.NO_WRAP));
            //Base64.encodeBase64String((client_id+":"+shared_secret_key).getBytes("UTF8")
            System.out.println("Authorization :" + headers.get("authorization"));
            System.out.println("Unencrypted body " + body);
            System.out.println("Encrypted body " + SecretKeyCrypt.encrypt(body));

            String response = HttpHelper.get("http://" + TerminalDetails.getBaseUrl() + "/gravity/api/oauth/token", SecretKeyCrypt.encrypt(body), "POST", headers);
            System.out.println("Response: " + response);
            String decResponse = SecretKeyCrypt.decrypt(response);
            System.out.println("Response received " + response + "\nResponse decrypted " + decResponse);
            TerminalDetails.token = json.fromJson(decResponse, Map.class).get("access_token") + "";
            if (!TerminalDetails.token.isEmpty()) {
                TerminalDetails.lastTokenRequestTime = System.currentTimeMillis();
            }
        } catch (Exception ex) {
            Logger.getLogger(HttpHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
