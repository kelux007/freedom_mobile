package com.a3lineng.softwaredev.freedom_app.Withdrawal;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.a3lineng.softwaredev.freedom_app.R;
import com.a3lineng.softwaredev.freedom_app.listeners.OnDestroyFragmentRequest;
import com.a3lineng.softwaredev.freedom_app.utilities.Contract;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import java.lang.ref.WeakReference;

import im.delight.android.location.SimpleLocation;

/**
 * A simple {@link Fragment} subclass.
 */
public class WithdrawalFragment2 extends Fragment implements WithdrawalView {
    static String strCustomerPin, strAgentPin, strAmount, strAccountNumber;
    static SimpleLocation location;
    static WithdrawalPresenter presenter;
    private static OnDestroyFragmentRequest mCallback;
    private static WeakReference<Context> contextWeakReference;
    private static WeakReference<Activity> activityWeakReference;
    Button btnProceed;
    ProgressBar progressBar;
    EditText etCustomerPin, etAgentPin, etAmount, etAccountNumber;

    public WithdrawalFragment2() {
        // Required empty public constructor
    }

    public static void callback(String otp) {
        presenter.validateWithdrawField2(otp, strAmount, strCustomerPin, strAgentPin, location);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_withdrawal2, container, false);
        contextWeakReference = new WeakReference<>(getContext());
        activityWeakReference = new WeakReference<>(getActivity());
        presenter = new WithdrawalPresenterImpl(this, new WithdrawalInteractorImpl(contextWeakReference));
        initializeWidgets(view);


        btnProceed.setOnClickListener(v -> {
            strAmount = etAmount.getText().toString().trim();
            strAgentPin = etAgentPin.getText().toString().trim();
            strCustomerPin = etCustomerPin.getText().toString().trim();


            /// presenter.requestOtp(strAccountNumber, strAgentPin, location);
            presenter.withdraw(strAccountNumber, strAmount, strCustomerPin, strAgentPin);
        });

        return view;
    }

    @Override
    public void alert2(String title, String message, DialogInterface.OnClickListener yesListener) {
        Utility.alert2(contextWeakReference.get(), title, message, yesListener);
    }

    @Override
    public void showProgress(String message) {
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        activityWeakReference.get().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void alert(String title, String message) {
        Utility.alert(contextWeakReference.get(), title, message, true);
    }


    @Override
    public void showToast(String message) {
        Utility.showToast(contextWeakReference.get(), message);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        activityWeakReference.get().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void updateProgress(String message) {

    }

    private void initializeWidgets(View view) {
        etAmount = view.findViewById(R.id.withdrawal_amount);
        etCustomerPin = view.findViewById(R.id.withdrawal_customerpin);
        etAgentPin = view.findViewById(R.id.withdrawal_agentpin);
        btnProceed = view.findViewById(R.id.withdrawal_proceed_button);
        progressBar = view.findViewById(R.id.progBar);
    }

    @Override
    public void showEnterOtpView(String message) {
        View viewInflated = LayoutInflater.from(contextWeakReference.get())
                .inflate(R.layout.fragment_otpp, (ViewGroup) getView(), false);
        Utility.showInputOTPDialog(contextWeakReference.get(), Contract.CASH_WITHDRAWAL,
                viewInflated, "Validate Transaction", message, false);
    }


}

