package com.a3lineng.softwaredev.freedom_app.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.a3lineng.softwaredev.freedom_app.AssetApplication;

public class SharedPreferencesHelper {
    public static final String PREF = "devicedetails";
    private static SharedPreferences sharedpreferences;

    public static boolean saveValue(Context context, String key, String value, String name) {
        try {
            setPreference(context, name);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(key, value);
            editor.apply();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean saveStan(String stanTosave, Context context) {
        try {
            saveValue(context, "stan", stanTosave, SharedPreferencesHelper.PREF);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static String getStringValue(Context context, String key, String name) {
        setPreference(context, name);
        return sharedpreferences.getString(key, ""); //"" is default value to return if key doesn't exist
    }

    public static boolean getBooleanValue(Context context, String key, String name) {
        setPreference(context, name);
        return sharedpreferences.getBoolean(key, false); //"" is default value to return if key doesn't exist
    }


    public static int getIntValue(Context context, String key, String name) {
        setPreference(context, name);
        return sharedpreferences.getInt(key, -1); //"" is default value to return if key doesn't exist
    }

    //prevents more unnecessary calls if already obtained
    private static void setPreference(Context context, String name) {
        if (sharedpreferences == null) {
            System.out.println("Shared Preferences is null");
            sharedpreferences = AssetApplication.getAppContext().getSharedPreferences(name, Context.MODE_PRIVATE);
        } else {
            System.out.println("Shared preference is not null");
        }
    }

    public static boolean saveBooleanValue(Context context, String name, String key, boolean value) {
        try {
            System.out.println("Got into save boolean value method");
            setPreference(context, name);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean(key, value);
            editor.apply();
        } catch (Exception e) {
            System.out.println("Observe exception" + e.getMessage());
            return false;
        }
        return true;
    }
}
