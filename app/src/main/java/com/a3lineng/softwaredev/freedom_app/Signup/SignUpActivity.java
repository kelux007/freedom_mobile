package com.a3lineng.softwaredev.freedom_app.Signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.a3lineng.softwaredev.freedom_app.R;
import com.a3lineng.softwaredev.freedom_app.Signin.SignInActivity;

public class SignUpActivity extends AppCompatActivity {

    private void navigateToLogin() {
        Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        if (savedInstanceState != null) {
            getFragmentManager().executePendingTransactions();
            android.app.Fragment fragmentById = getFragmentManager().findFragmentById(R.id.fragment_container);


            if (fragmentById != null) {
                getFragmentManager().beginTransaction().remove(fragmentById).commit();
            }
        }

        SignupFragment signupFragment = new SignupFragment();
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.fragment_container, signupFragment, "setup").commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
