package com.a3lineng.softwaredev.freedom_app.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import java.util.HashMap;

/**
 * Created by OluwatosinA on 21-Mar-18.
 */

public class TerminalDetails {
    public static final String RESP_ASYNC_RESP_POS = "POS.ASYNC.RESPONSE";
    public static final String PREF = "terminaldetails";
    public static final String IS_SETTINGS_CHANGED = "is_settings_changed";
    public static final String RESP_RESP_SEEN = "POS.ASYNC.RESPONSE.SEEN";
    //public static String gravityApiBasePath = "/gravity/api/TransactionDispatcherSingleISO?isomsg=";
    public static final String gravityApiBasePath2 = "/gravity/api/";
    public static final String RESP_ACTIVATE_POS = "POS.ACTIVATE";
    public static final String transportKey = "A8985E04623ED3AD75F213FBADC70E2C";
    public static final String isoPinKey = "0032093B615439AA4737EA4FDEA0FD3CBCE7";
    public static String BASE_URL = "http://10.2.2.47:8080";
    //public static String gravityBasePath = "/api";
    public static String gravityBasePath = "/gravity/api";
    public static String nctoBasePath = "/ncto";
    public static String agentsetup = "/agentSetup";
    public static String shared_secret_key;
    public static String branchName;
    public static String branchCode;
    public static String client_id;
    public static long lastTokenRequestTime;
    public static String ipport = "ws://localhost:9080/";//"10.4.4.31:9090/gravity";// ""+ipport+"";
    //public static String tellerID = "178";
    public static String tellerID;
    public static String merchantId = "";
    public static String sessionKey = "";
    //public static String terminalId = "W888002F";
    public static String terminalId = "W888002F";
    //public static String IP = "mecurepay.com";
    public static String IP = "10.4.4.31";
    public static String Port = "9080";
    public static String gravityApiBasePath = "/gravity/api/TransactionDispatcherSingleISO?isomsg=";
    public static String updateUrl = gravityApiBasePath2 + "checkUpdates?version=";
    public static String HTTP_VERB_POST = "POST";
    public static String HTTP_VERB_GET = "GET";
    //public static String IP = "10.9.8.206:8080";
    //public static String IP = "41.219.149.51:9080";
    //public static String IP = "10.2.2.47:9080";
    //public static final String WEB_SOCKET_URL = "ws://" + IP + "/api/gravitywebsocket";
    public static String header;
    public static String footer;
    public static String line1;
    public static String line2;
    public static String masterKey;
    public static String initiator = TerminalDetails.tellerID;
    //public static String initiator = "";
    public static String beneficiary;
    public static String token;
    public static String agentName;
    public static String userName;
    public static String staffId;
    public static String owner;
    public static String agentId;
    public static String deviceId = TerminalDetails.terminalId;
    public static long lockoutTime;
    public static String clientId;
    public static String secretKey;
    public static String agentLogon = "/logon";
    public static String oauth = "/oauth/token";
    public static String deviceSetupStatus;
    public static String balance;
    public static String OAuth_Token;
    public static String token_expires_in;
    public static String getDetails = "/getcustomerdetails";
    public static String disburseFund = "/disburseFund";
    public static boolean isTokenExpired = false;
    public static String verifyName = "/validateCustomerAccount";
    public static String deposit = "/savings";
    public static String uploadCustomers = "/uploadcustomers";
    public static String uploadTransactions = "";
    public static String disbursementPlan = "/disbursementPlan";
    private static SharedPreferences sharedpreferences;
    public static String customerRegistration = "/customerRegistration";
    public static String generateTransactionToken = "/generateTransactionToken";
    public static String generateOTP = "/generateOTP";
    public static String withdrawal = "/withdrawals";
    public static String intraBankTransfer = "/intraBankTransfer";

    public static String getIncrementedStan(Context context) {
        try {
            System.out.println("Observe well .... " + getStringValue(context, "stan", TerminalDetails.PREF));
            return String.format("%06d", Integer.parseInt(getStringValue(context, "stan", TerminalDetails.PREF)) + 1);
        } catch (NumberFormatException e) {
            return "000001";
        }
    }

    public static boolean incrementStan(Context context, String... stan) {
        try {
            String stanTosave = "";
            if (stan.length == 0) {
                stanTosave = getIncrementedStan(context);
            } else {
                stanTosave = (Integer.parseInt(stan[0]) + 1) + "";
            }
            saveValue(context, "stan", stanTosave, TerminalDetails.PREF);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean saveStan(String stanTosave, Context context) {
        try {
            saveValue(context, "stan", stanTosave, TerminalDetails.PREF);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean saveValue(Context context, String key, String value, String name) {
        try {
            setPreference(context, name);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(key, value);
            editor.apply();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static String getStringValue(Context context, String key, String name) {
        setPreference(context, name);
        return sharedpreferences.getString(key, ""); //"" is default value to return if key doesn't exist
    }


    public static int getIntValue(Activity context, String key, String name) {
        setPreference(context, name);
        return sharedpreferences.getInt(key, -1); //"" is default value to return if key doesn't exist
    }

    //prevents more unnecessary calls if already obtained
    private static void setPreference(Context context, String name) {
        if (sharedpreferences == null) {
            sharedpreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        }
    }

    public static boolean saveBooleanValue(Activity context, String name, String key, boolean value) {
        try {
            setPreference(context, name);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean(key, value);
            editor.apply();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static String getBaseUrl() {
        return IP + ":" + Port;
    }

    public static boolean saveUserLastLocation(Activity context, String name, String lon, String lat) {
        try {
            setPreference(context, name);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("lon", lon);
            editor.putString("lat", lat);
            editor.apply();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static HashMap getUserLastLocation(Activity context, String key, String name) {
        setPreference(context, name);
        HashMap location = new HashMap();
        location.put("lon", sharedpreferences.getString("lon", "0.0")); //"0.0" is default value to return if key doesn't exist)
        location.put("lat", sharedpreferences.getString("lat", "0.0"));
        return location;
    }

    public static int getVersionCode(Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            return pi.versionCode;
        } catch (PackageManager.NameNotFoundException ex) {
        }
        return 0;
    }

    public static String getVersionName(Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            return pi.versionName;
        } catch (PackageManager.NameNotFoundException ex) {
        }
        return null;
    }

    public static PackageInfo getPackageInfo(Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            return pi;
        } catch (PackageManager.NameNotFoundException ex) {
        }
        return null;
    }

    public static boolean getUserSession(Activity context, String key) {
        setPreference(context, TerminalDetails.PREF);
        return sharedpreferences.getBoolean(key, false);
    }

}
