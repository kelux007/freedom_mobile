package com.a3lineng.softwaredev.freedom_app.Withdrawal;

import com.a3lineng.softwaredev.freedom_app.base_interactors.BasePresenter;

import im.delight.android.location.SimpleLocation;

public interface WithdrawalPresenter extends BasePresenter {
    void withdraw(String accountNumber, String amount, String customerPin, String agentPin);

    void withdraw2(String amount, String customerPin, String agentPin);

    void validateWithdrawField(String bankName, String otp, String amount, String customerPin, String agentPin, SimpleLocation location);

    void validateWithdrawField2(String otp, String amount, String customerPin, String agentPin, SimpleLocation location);

    void verifyName(String accountName, SimpleLocation location);

    void requestOtp(String accountNumber, String agentPin, SimpleLocation location);

    void onDestroy();

    void verifyBenName(String bankCode, String trim, SimpleLocation location);

}
