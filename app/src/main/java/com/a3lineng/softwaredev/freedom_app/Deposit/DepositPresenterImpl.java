package com.a3lineng.softwaredev.freedom_app.Deposit;

import android.content.DialogInterface;

import im.delight.android.location.SimpleLocation;

public class DepositPresenterImpl implements DepositPresenter, DepositInteractor.OnDepositFinishedListener {
    DepositInteractor depositInteractor;
    DepositView depositView;

    public DepositPresenterImpl(DepositInteractor depositInteractor, DepositView depositView) {
        this.depositInteractor = depositInteractor;
        this.depositView = depositView;
    }

    @Override
    public void onError(String title, String message) {
        if(depositView != null){
            depositView.hideProgress();
            depositView.showAlert(title, message);
        }
    }

    @Override
    public void onSuccess(String title, String message, DialogInterface.OnClickListener yeslistener) {
        if(depositView != null){
            depositView.hideProgress();
            depositView.showAlert4(title, message, yeslistener);
        }
    }

    @Override
    public void onNameEnquirySuccess(String accountName) {
        if(depositView != null){
            depositView.hideProgress();
            depositView.updateAccountName(accountName);
        }
    }

    @Override
    public void onUpdateProgressMessage(String message) {
        if(depositView != null){
            depositView.updateProgress(message);
        }
    }

    @Override
    public void onWrongPinEntered(String title, String message, DialogInterface.OnClickListener cancel, DialogInterface.OnClickListener retry) {
        if(depositView != null){

        }
    }

    @Override
    public void onStartDeposit(String message) {
        if(depositView != null){
            depositView.showProgress(message);
        }
    }

    @Override
    public void destroyFragment() {
        if(depositView != null){
            depositView.destroyFragment();
        }
    }

    @Override
    public void onDisplayAlert(String title, String message, DialogInterface.OnClickListener yeslistener, DialogInterface.OnClickListener nolistener) {

    }

    @Override
    public void validateDepositField(String accountNumber, String agentPin, String amount, String narration, String receiptNumber, SimpleLocation location) {
        if(depositView != null){
            depositInteractor.deposit(accountNumber, amount, narration, receiptNumber, agentPin, this);
        }
    }

    @Override
    public void validateName(String accountNumber, SimpleLocation location) {
        if(depositView != null){
            depositInteractor.nameEnquiry(accountNumber, location, this);
        }
    }

    @Override
    public void onDestroy() {
        if(depositView != null){
            depositView = null;
            depositInteractor = null;
        }
    }
}
