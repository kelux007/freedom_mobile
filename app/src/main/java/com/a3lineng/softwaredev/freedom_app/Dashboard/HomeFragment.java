package com.a3lineng.softwaredev.freedom_app.Dashboard;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.a3lineng.softwaredev.freedom_app.R;
import com.a3lineng.softwaredev.freedom_app.TabView_Fragments.AccountsTabFragment;
import com.a3lineng.softwaredev.freedom_app.TabView_Fragments.BillPaymentTabFragment;
import com.a3lineng.softwaredev.freedom_app.TabView_Fragments.DepositTabFragment;
import com.a3lineng.softwaredev.freedom_app.TabView_Fragments.TransferTabFragment;
import com.a3lineng.softwaredev.freedom_app.TabView_Fragments.WithdrawalTabFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);

        initializeWidgets(view);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }

    private void initializeWidgets(View view) {
        tabLayout = view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.viewpager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new AccountsTabFragment(), "Accounts");
        adapter.addFrag(new WithdrawalTabFragment(),"Withdrawals");
        adapter.addFrag(new DepositTabFragment(), "Deposits");
        adapter.addFrag(new TransferTabFragment(),"Transfers");
        adapter.addFrag(new BillPaymentTabFragment(), "Bill Payments");

        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
