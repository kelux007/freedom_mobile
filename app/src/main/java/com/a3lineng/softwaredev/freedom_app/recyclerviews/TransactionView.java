package com.a3lineng.softwaredev.freedom_app.recyclerviews;

/**
 * Created by Lincoln on 15/01/16.
 */
public class TransactionView {
    private String name;

    public TransactionView() {
    }

    public TransactionView(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
