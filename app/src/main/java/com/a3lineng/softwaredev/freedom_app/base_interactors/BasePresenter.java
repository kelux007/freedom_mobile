package com.a3lineng.softwaredev.freedom_app.base_interactors;

public interface BasePresenter {
    void onDestroy();
}
