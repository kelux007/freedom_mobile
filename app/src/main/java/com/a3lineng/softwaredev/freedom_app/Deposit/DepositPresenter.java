package com.a3lineng.softwaredev.freedom_app.Deposit;

import im.delight.android.location.SimpleLocation;

public interface DepositPresenter {
    void validateDepositField(String accountNumber, String agentPin, String amount,  String narration, String customerPin, SimpleLocation location);

    void validateName(String accountNumber, SimpleLocation location);

    void onDestroy();
}
