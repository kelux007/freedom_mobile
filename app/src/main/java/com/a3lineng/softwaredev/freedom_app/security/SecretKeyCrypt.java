/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.a3lineng.softwaredev.freedom_app.security;

import android.util.Base64;
import android.util.Log;

import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

/**
 * @author Giulio
 */
public class SecretKeyCrypt {

    private static SecretKey key = fromKeyStore();

    public SecretKeyCrypt(String message) {
        try {
            System.out.println("clear message: " + message);


            String encrypted = encrypt(message);
            System.out.println("encrypted message: " + encrypted);

            String decrypted = decrypt(encrypted);
            System.out.println("decrypted message: " + decrypted);

        } catch (CryptographyException e) {
            e.printStackTrace();
        }
    }

    public static void test() {
        String hash = "";
        try {
            hash = encrypt("test");
        } catch (CryptographyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        for (int i = 0; i < 100; i++)
            Log.i("SECRET KEY CRYPT ", hash);
    }

    /**
     * Generates the encryption key. using "AES" algorithm
     *
     * @throws NoSuchAlgorithmException
     */

    public static String encrypt(String message) throws CryptographyException {
        // Get a cipher object.
        String base64;
        try {
            Log.e("KESTORE CONTENT = ", (key.getEncoded() == null) + "");
            //SecretKeySpec skeySpec = new SecretKeySpec(key.getEncoded(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            //cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

            // Gets the raw bytes to encrypt, UTF8 is needed for
            // having a standard character set
            byte[] stringBytes = message.getBytes("UTF8");

            // encrypt using the cypher
            byte[] raw = cipher.doFinal(stringBytes);

            base64 = Base64.encodeToString(raw, Base64.NO_WRAP);
        } catch (Exception e) {
            Log.e("CRYPTO ERROR", Utility.stringify(e));
            throw new CryptographyException(e);
        }
        // converts to base64 for easier display.

        return base64;
    }

    public static String decrypt(String encrypted) throws CryptographyException {
        String clear;
        // Get a cipher object.
        try {

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key);

            // decode the BASE64 coded string message
            byte[] raw = Base64.decode(encrypted, Base64.NO_WRAP);

            //decode the message
            byte[] stringBytes = cipher.doFinal(raw);

            //converts the decoded message to a String
            clear = new String(stringBytes, "UTF8");
        } catch (Exception e) {
            throw new CryptographyException(e);
        }
        return clear;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        new SecretKeyCrypt("this is a test crypt");
    }

    private static SecretKey fromKeyStore() {
        try {
            return KeyStoreWrapper.getKey();
        } catch (Exception ex) {
            Logger.getLogger(SecretKeyCrypt.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
