package com.a3lineng.softwaredev.freedom_app.Signin;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.a3lineng.softwaredev.freedom_app.httphelper.Backend;
import com.a3lineng.softwaredev.freedom_app.httphelper.Token;
import com.a3lineng.softwaredev.freedom_app.security.CryptographyException;
import com.a3lineng.softwaredev.freedom_app.security.SecretKeyCrypt;
import com.a3lineng.softwaredev.freedom_app.utilities.TerminalDetails;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

import im.delight.android.location.SimpleLocation;

public class SigninInteractorImpl implements SigninInteractor {
    private WeakReference<Context> contextWeakReference;

    SigninInteractorImpl(WeakReference<Context> contextWeakReference) {
        this.contextWeakReference = contextWeakReference;
    }

    @Override
    public void signinAgent(String username, String password, SimpleLocation location, OnSigninFinished listener) {
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            listener.onError("Error", "All fields are required");
        } else {
            if (Utility.isNetworkConnected(contextWeakReference.get())) {
                new SignIn(username, password, contextWeakReference, listener).execute();
            } else {
                listener.onError("Error", "Internet connection not available");
            }
        }
    }

    static class SignIn extends AsyncTask<Void, Void, String> {

        private final SigninInteractor.OnSigninFinished listener;
        private final String username, password;
        private final WeakReference<Context> contextWeakReference;

        SignIn(String username, String password, WeakReference<Context> contextWeakReference, SigninInteractor.OnSigninFinished listener) {
            this.username = username;
            this.password = password;
            this.listener = listener;
            this.contextWeakReference = contextWeakReference;
        }

        @Override
        protected String doInBackground(Void... voids) {
            String isoResponse = "Unable to authenticate at this time";
            try {
                if(TerminalDetails.isTokenExpired){
                    new Token.GetToken(contextWeakReference).execute();
                }

                isoResponse = Backend.logon(contextWeakReference.get(), username, password);

            } catch (Exception e) {
                Log.e("ERROR ERROR", Utility.stringify(e));
            }
            return isoResponse;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listener.onStart("...Downloading Security Keys");
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equals("return")) {
                return;
            }

            if (result.equals("Unable to authenticate at this time")) {
                listener.onError("Network", result);
                return;
            }
            Log.e("SERVER RESPONSE", result);
            try {
                result = SecretKeyCrypt.decrypt(result);
                Log.e("\nSR decrypted ", result);
            } catch (CryptographyException e1) {
                e1.printStackTrace();
            }
            JSONObject responseIso = null;
            try {
                responseIso = new JSONObject(result);
            } catch (Exception e) {
                listener.onError("Agent Logon", "Bad Data returned from server");
                return;
            }


            String respCode = "";
            String respDesc = "";

            try {
                respCode = responseIso.getString("respCode");
                respDesc = responseIso.getString("respDescription");
            } catch (Exception e) {
            }

            switch (respCode) {
                case "00":

                    listener.onSuccess();
                    break;
                case "109":
                    TerminalDetails.isTokenExpired = true;
                    new SignIn(username, password, contextWeakReference, listener).execute();
                    break;
                default:
                    String response = respDesc;
                    response = response.equals("") ? "Bad Response Code :" + respCode + " returned from the server" : response;
                    listener.onError("Logon status", response);
                    break;
            }

        }
    }
}
