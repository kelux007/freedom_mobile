package com.a3lineng.softwaredev.freedom_app.Change_Password;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.a3lineng.softwaredev.freedom_app.R;

import java.lang.ref.WeakReference;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class PasswordChange extends Fragment {

    EditText editTextoldpassword, editTextnewpassword, editTextagentpin;
    Button buttonproceed;
    LinearLayout dashboard, services, profile;
    Unbinder unbinder;
    private WeakReference<Context> contextWeakReference;

    public PasswordChange() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_password_change, container, false);

        contextWeakReference = new WeakReference<>(getContext());
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        getActivity().setTitle(getString(R.string.pass_change));
        getActivity().setTitleColor(android.R.color.white);
        setHasOptionsMenu(true);
        initializeWidgets(view);


        buttonproceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("password changed");

            }
        });


        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    private void initializeWidgets(View view) {
        editTextoldpassword = view.findViewById(R.id.changepassword_oldpassword);
        editTextnewpassword = view.findViewById(R.id.changepassword_newpassword);
        editTextagentpin = view.findViewById(R.id.changepassword_agentpin);
        buttonproceed = view.findViewById(R.id.changepassword_proceed);
    }

}
