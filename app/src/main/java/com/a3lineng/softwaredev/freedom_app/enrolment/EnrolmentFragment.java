package com.a3lineng.softwaredev.freedom_app.enrolment;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.a3lineng.softwaredev.freedom_app.R;
import com.a3lineng.softwaredev.freedom_app.listeners.OnDestroyFragmentRequest;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import java.lang.ref.WeakReference;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnrolmentFragment extends Fragment implements EnrolmentView {
    private EditText etName, etBvn, etMotherMaidenName, etDob, etPhoneNumber;
    private String name, bvn, motherMaidenName, dob, phone;
    private Button btnProceed;
    private ProgressBar progressBar;
    private WeakReference<Context> contextWeakReference;
    private WeakReference<Activity> activityWeakReference;
    EnrolmentPresenter presenter;
    private static OnDestroyFragmentRequest mCallback;
    private static WeakReference<Fragment> fragmentWeakReference;

    public EnrolmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_enrolment, container, false);
        contextWeakReference = new WeakReference<>(getContext());
        activityWeakReference = new WeakReference<>(getActivity());
        fragmentWeakReference = new WeakReference<>(this);

        presenter = new EnrolmentPresenterImpl(this, new EnrolmentInteractorImpl(contextWeakReference));
        initializeWidgets(view);

        btnProceed.setOnClickListener(v -> {
            name = etName.getText().toString().trim();
            bvn = etBvn.getText().toString().trim();
            motherMaidenName = etMotherMaidenName.getText().toString().trim();
            dob = etDob.getText().toString().trim();
            phone = etPhoneNumber.getText().toString().trim();

            presenter.validateEnrolmentField(name, bvn, motherMaidenName, dob, phone);
        });
        return view;
    }

    private void initializeWidgets(View view) {
        etName = view.findViewById(R.id.etName);
        etBvn = view.findViewById(R.id.etBvn);
        etMotherMaidenName = view.findViewById(R.id.etMotherMaidenName);
        etDob = view.findViewById(R.id.etDob);
        etPhoneNumber = view.findViewById(R.id.etPhoneNumber);
        progressBar = view.findViewById(R.id.progBar);
        btnProceed = view.findViewById(R.id.btnProceed);
    }

    @Override
    public void destroyFragment() {
        //destroyFragment(fragmentWeakReference.get());
    }

    @Override
    public void alert(String title, String message, DialogInterface.OnClickListener yeslistener) {
        Utility.alert2(contextWeakReference.get(), title, message, yeslistener, true);
    }

    @Override
    public void showProgress(String message) {
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        activityWeakReference.get().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void alert(String title, String message) {
        Utility.alert(contextWeakReference.get(), title, message, true);
    }

    @Override
    public void showToast(String message) {
        Utility.showToast(contextWeakReference.get(), message);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        activityWeakReference.get().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void updateProgress(String message) {

    }

    private static void destroyFragment(Fragment fragment) {
        // Send the event to the host activity
        mCallback.onFragmentDestroy(fragment);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnDestroyFragmentRequest) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
}
