package com.a3lineng.softwaredev.freedom_app.service;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.a3lineng.softwaredev.freedom_app.utilities.LocationUtility;

//import com.a3lineng.softwaredev.freedom_app.utilities.LocationUtility;

public class LocationService extends Service implements LocationListener {
    public static final long TIME_BTW_LOCATION_UPDATES = 60 * 1000; //millseconds i.e 1 Minutes
    public static final long DISTANCES_BTW_LOCATION_UPDATES = 10;
    public static final int PERMISSION_LOCATION = 500;
    public double latitude; // latitude
    public double longitude; // longitude
    // flag for GPS status
    boolean isGPSEnabled = false;
    // flag for network status
    boolean isNetworkEnabled = false;
    // flag for GPS status
    boolean canGetLocation = false;
    Location location; // location
    private Context mContext;
    private LocationManager locationManager;

    public LocationService() {
    }

    public LocationService(Context context) {
        this.mContext = context;
        getLocation();
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            if (!isGPSEnabled && !isNetworkEnabled) {
                Log.i("info", " no network provider is enabled");
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.

                            ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION);
                        } else {
                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TIME_BTW_LOCATION_UPDATES, DISTANCES_BTW_LOCATION_UPDATES, this);
                            Log.d("Network", "Network");
                            if (locationManager != null) {
                                location = locationManager
                                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                Log.i("location", "" + longitude);
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    LocationUtility.lat = String.valueOf(latitude);
                                    longitude = location.getLongitude();
                                    LocationUtility.lon = String.valueOf(longitude);
                                }
                            }
                        }
                    } else {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TIME_BTW_LOCATION_UPDATES, DISTANCES_BTW_LOCATION_UPDATES, this);
                        Log.d("Network", "Network");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            Log.i("location", "" + longitude);
                            if (location != null) {
                                latitude = location.getLatitude();
                                LocationUtility.lat = String.valueOf(latitude);
                                longitude = location.getLongitude();
                                LocationUtility.lon = String.valueOf(longitude);
                            }
                        }
                    }
                }
                if (isGPSEnabled) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.

                            ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION);
                        } else {
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                            Log.d("Network", "Network");
                            if (locationManager != null) {
                                location = locationManager
                                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                Log.i("location", "" + longitude);
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    LocationUtility.lat = String.valueOf(latitude);
                                    longitude = location.getLongitude();
                                    LocationUtility.lon = String.valueOf(longitude);
                                }
                            }
                        }
                    } else {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TIME_BTW_LOCATION_UPDATES, DISTANCES_BTW_LOCATION_UPDATES, this);
                        Log.d("Network", "Network");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            Log.i("location", "" + longitude);
                            if (location != null) {
                                latitude = location.getLatitude();
                                LocationUtility.lat = String.valueOf(latitude);
                                longitude = location.getLongitude();
                                LocationUtility.lon = String.valueOf(longitude);
                            }
                        }
                    }
                }


            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }


    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(LocationService.this);
        }
    }

    /**
     * Function to get latitude
     */
    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     */
    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    public void initializeLocation() {
        if (location != null) {
            LocationUtility.lon = String.valueOf(location.getLongitude());
            LocationUtility.lat = String.valueOf(location.getLatitude());
        } else {
            getLocation();
        }
    }

    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            Log.i("SuperMap", "Location changed : Lat: " + location.getLatitude() + " Lng: " + location.getLongitude());

            latitude = location.getLatitude();
            longitude = location.getLongitude();
            LocationUtility.lon = String.valueOf(longitude);
            LocationUtility.lat = String.valueOf(latitude);
            Log.i("latitude,longitude", "" + latitude + "," + longitude);


        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


}

