package com.a3lineng.softwaredev.freedom_app.security;

public class CryptographyException extends Exception {

    private static final long serialVersionUID = 9173319023447640503L;

    String message;
    Object obj;

    public CryptographyException(String message) {
        this.message = message;
    }

    public CryptographyException(Exception e) {
        this.message = e.getMessage();
        super.setStackTrace(e.getStackTrace());
    }

    public CryptographyException(String message, Object obj) {
        this.message = message;
        this.obj = obj;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Object getObject() {
        return obj;
    }
}
