package com.a3lineng.softwaredev.freedom_app.base_interactors;

public interface BaseView {
    void showProgress(String message);
    void alert(String title, String message);
    void showToast(String message);
    void hideProgress();
    void updateProgress(String message);
}
