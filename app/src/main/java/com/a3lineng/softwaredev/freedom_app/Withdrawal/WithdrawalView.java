package com.a3lineng.softwaredev.freedom_app.Withdrawal;

import android.content.DialogInterface;

import com.a3lineng.softwaredev.freedom_app.base_interactors.BaseView;

public interface WithdrawalView extends BaseView {
    void alert2(String title, String message, DialogInterface.OnClickListener yesListener);

    void updateProgress(String message);


    void showEnterOtpView(String message);

    void showProgress(String message);

    void hideProgress();

}
