package com.a3lineng.softwaredev.freedom_app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.a3lineng.softwaredev.freedom_app.Signin.SignInActivity;
import com.a3lineng.softwaredev.freedom_app.Signup.SignUpActivity;

import java.lang.ref.WeakReference;

public class WelcomeActivity extends Activity {
    Button buttonsignup;
    Button buttonsignin;
    private WeakReference<Context> contextWeakReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        contextWeakReference = new WeakReference<>(getBaseContext());

    }

    public void open_signin(View view) {
        Intent intent = new Intent(contextWeakReference.get(), SignInActivity.class);
        startActivity(intent);
    }

    public void open_signup(View view) {
        Intent intent = new Intent(contextWeakReference.get(), SignUpActivity.class);
        startActivity(intent);
    }
}
