package com.a3lineng.softwaredev.freedom_app.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.a3lineng.softwaredev.freedom_app.model.Beneficiary;
import com.a3lineng.softwaredev.freedom_app.model.ExportBeneficiary;
import com.a3lineng.softwaredev.freedom_app.model.Schedules;
import com.a3lineng.softwaredev.freedom_app.model.Transaction;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    private static final String TAG = DBHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "beneficiaries_db";

    private static final int DATABASE_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DBHelper(Context context, int version) {
        super(context, DATABASE_NAME, null, version);
    }

//    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
//        super(context, name, factory, version);
//    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if (db != null) {
            db.execSQL(ExportBeneficiary.CREATE_TABLE_BENEFICIARY);
            db.execSQL(Transaction.CREATE_TABLE_TRANSACTION);
            db.execSQL(Schedules.CREATE_TABLE_SCHEDULES);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Beneficiary.TABLE_BENEFICIARY);
        db.execSQL("DROP TABLE IF EXISTS " + Transaction.TABLE_TRANSACTION);
        // Create tables again
        onCreate(db);
    }

    public long insertBeneficiary(Beneficiary beneficiary) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(Beneficiary.COLUMN_BEN_FIRSTNAME, beneficiary.getFirstName());
        cv.put(Beneficiary.COLUMN_BEN_LASTNAME, beneficiary.getLastName());
        cv.put(Beneficiary.COLUMN_BEN_MIDDLENAME, beneficiary.getMiddleName());
        cv.put(Beneficiary.COLUMN_BEN_GENDER, beneficiary.getGender());
        cv.put(Beneficiary.COLUMN_BEN_PHONE, beneficiary.getPhoneNumber());
        cv.put(Beneficiary.COLUMN_BEN_DOB, beneficiary.getDob());
        cv.put(Beneficiary.COLUMN_BEN_PIN, beneficiary.getCustomerPin());
        cv.put(Beneficiary.COLUMN_ALTERNATE_FIRSTNAME, beneficiary.getAltFirstName());

        cv.put(Beneficiary.COLUMN_ALTERNATE_LASTNAME, beneficiary.getAltLastName());
        cv.put(Beneficiary.COLUMN_ALTERNATE_MIDNAME, beneficiary.getAltMiddleName());
        cv.put(Beneficiary.COLUMN_ALTERNATE_DOB, beneficiary.getAltDob());
        cv.put(Beneficiary.COLUMN_ALTERNATE_GENDER, beneficiary.getAltGender());

        cv.put(Beneficiary.COLUMN_LGA, beneficiary.getLga());
        cv.put(Beneficiary.COLUMN_STATE, beneficiary.getState());
        cv.put(Beneficiary.COLUMN_WARD, beneficiary.getWard());

        cv.put(Beneficiary.COLUMN_BEN_PICTURE, beneficiary.getPicture());
        cv.put(Beneficiary.COLUMN_ALTERNATE_PICTURE, beneficiary.getAltPicture());

        long id = db.insert(Beneficiary.TABLE_BENEFICIARY, null, cv);

        return id;
    }

    public long insertExportBeneficiary(ExportBeneficiary exportBeneficiary) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(ExportBeneficiary.COLUMN_BEN_NAME, exportBeneficiary.getCaregiverName());
        cv.put(ExportBeneficiary.COLUMN_BEN_GENDER, exportBeneficiary.getCaregiverGender());
        cv.put(ExportBeneficiary.COLUMN_BEN_REF_NUM, exportBeneficiary.getCaregiverRefNum());
        cv.put(ExportBeneficiary.COLUMN_BEN_AGE, exportBeneficiary.getCaregiverAge());
        cv.put(ExportBeneficiary.COLUMN_BEN_PHONE, exportBeneficiary.getCaregiverPhone());
        cv.put(ExportBeneficiary.COLUMN_ALTERNATE_PHONE, exportBeneficiary.getAlternatePhone());
        cv.put(ExportBeneficiary.COLUMN_ALTERNATE_NAME, exportBeneficiary.getAlternateName());
        cv.put(ExportBeneficiary.COLUMN_ALTERNATE_GENDER, exportBeneficiary.getAlternateGender());
        cv.put(ExportBeneficiary.COLUMN_ALTERNATE_REF_NUM, exportBeneficiary.getAlternateRefNum());
        cv.put(ExportBeneficiary.COLUMN_ALTERNATE_AGE, exportBeneficiary.getAlternateAge());

        cv.put(ExportBeneficiary.COLUMN_ADDRESS, exportBeneficiary.getAddress());
        cv.put(ExportBeneficiary.COLUMN_STATE, exportBeneficiary.getState());
        cv.put(ExportBeneficiary.COLUMN_CITY, exportBeneficiary.getCity());
        cv.put(ExportBeneficiary.COLUMN_LGA, exportBeneficiary.getLga());
        cv.put(ExportBeneficiary.COLUMN_WARD, exportBeneficiary.getWard());
        cv.put(ExportBeneficiary.COLUMN_COMMUNITY, exportBeneficiary.getCommunity());
        cv.put(ExportBeneficiary.COLUMN_BEN_PICTURE, exportBeneficiary.getCaregiverBase64Pic());
        cv.put(ExportBeneficiary.COLUMN_ALTERNATE_PICTURE, exportBeneficiary.getAlternateBase64Pic());
        cv.put(ExportBeneficiary.COLUMN_HOUSE_HOLD_NO, exportBeneficiary.getHouseHoldNum());

        return db.insert(ExportBeneficiary.TABLE_BENEFICIARY, null, cv);
    }

    public long insertSchedule(Schedules schedules) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(Schedules.COLUMN_AMOUNT, schedules.getAmount());
        System.out.println("Observe inserted amount : " + schedules.getAmount());
        cv.put(Schedules.COLUMN_HOUSEHOLD_NUM, schedules.getHouseHoldNum());
        cv.put(Schedules.COLUMN_BEN_PIN, schedules.getPin());
        cv.put(Schedules.COLUMN_WALLETID, schedules.getWalletId());

        return db.insert(Transaction.TABLE_TRANSACTION, null, cv);
    }

//    public Schedules getScheduleByWalletId(String walletId) {
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(Schedules.TABLE_SCHEDULES,
//                null,
//                Schedules.COLUMN_WALLETID + "=?",
//                new String[]{walletId}, null, null, null, null);
//
//        if (cursor != null)
//            cursor.moveToFirst();
//        ExportBeneficiary exportBeneficiary = getExportBeneficiaryByWalletId(walletId);
//        Schedules schedules = new Schedules(
//                cursor.getInt(cursor.getColumnIndex(Schedules.COLUMN_BEN_ID)),
//                cursor.getString(cursor.getColumnIndex(Schedules.COLUMN_AMOUNT)),
//                cursor.getString(cursor.getColumnIndex(Schedules.COLUMN_WALLETID),
//                        cursor.getString(cursor.getColumnIndex(Schedules.COLUMN_HOUSEHOLD_NUM),
//                                cursor.getString(cursor.getColumnIndex(Schedules.COLUMN_BEN_PIN),
//                                        cursor.getString(cursor.getColumnIndex(Schedules.COLUMN_HOUSEHOLD_NUM));
//
//        // close the db connection
//        cursor.close();
//
//        return note;
//    }

//    private ExportBeneficiary getExportBeneficiaryByWalletId(String walletId) {
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(ExportBeneficiary.TABLE_BENEFICIARY,
//                null,
//                ExportBeneficiary.COLUMN_HOUSE_HOLD_NO + "=?",
//                new String[]{walletId}, null, null, null, null);
//
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        ExportBeneficiary beneficiary = new ExportBeneficiary(
//                cursor.getString(cursor.getColumnIndex(ExportBeneficiary.))
//        )
//    }

    public long insertTransaction(Transaction transaction) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(Transaction.COLUMN_HOUSEHOLD_NUM, transaction.getHouseHoldNum());
        System.out.println("Observe inserted ref number : " + transaction.getHouseHoldNum());
        cv.put(Transaction.COLUMN_AMOUNT, transaction.getAmount());
        cv.put(Transaction.COLUMN_LONGITUDE, transaction.getLongitude());
        cv.put(Transaction.COLUMN_LATITUDE, transaction.getLatitude());
        cv.put(Transaction.COLUMN_COLLECTEDBY, transaction.getCollectedBy());
        cv.put(Transaction.COLUMN_PIN, transaction.getPin());
        cv.put(Transaction.COLUMN_AGENT_PIN, transaction.getAgent());

        return db.insert(Transaction.TABLE_TRANSACTION, null, cv);
    }

    public List<Transaction> getAllTransaction() {
        List<Transaction> transactions = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + Transaction.TABLE_TRANSACTION + " ORDER BY " +
                Transaction.COLUMN_TRANSACTION_ID + " DESC";

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Transaction transaction = new Transaction();
                transaction.setHouseHoldNum(String.valueOf(cursor.getString(cursor.getColumnIndex(Transaction.COLUMN_HOUSEHOLD_NUM))));
                transaction.setAmount(cursor.getString(cursor.getColumnIndex(Transaction.COLUMN_AMOUNT)));
                transaction.setLongitude(cursor.getString(cursor.getColumnIndex(Transaction.COLUMN_LONGITUDE)));
                transaction.setLatitude(cursor.getString(cursor.getColumnIndex(Transaction.COLUMN_LATITUDE)));
                transaction.setPin(cursor.getString(cursor.getColumnIndex(Transaction.COLUMN_PIN)));
                transaction.setAgent(cursor.getString(cursor.getColumnIndex(Transaction.COLUMN_AGENT_PIN)));
                transaction.setCollectedBy(cursor.getString(cursor.getColumnIndex(Transaction.COLUMN_COLLECTEDBY)));

                transactions.add(transaction);
            } while (cursor.moveToNext());
        }

        // close db connection
        cursor.close();
        db.close();

        // return notes list
        return transactions;
    }

    public List<Beneficiary> getAllBeneficiaries() {
        List<Beneficiary> beneficiaries = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + Beneficiary.TABLE_BENEFICIARY + " ORDER BY " +
                Beneficiary.COLUMN_BEN_ID + " DESC";

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Beneficiary beneficiary = new Beneficiary();
                beneficiary.setId(String.valueOf(cursor.getInt(cursor.getColumnIndex(Beneficiary.COLUMN_BEN_ID))));
                beneficiary.setFirstName(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_BEN_FIRSTNAME)));
                beneficiary.setLastName(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_BEN_LASTNAME)));
                beneficiary.setMiddleName(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_BEN_MIDDLENAME)));
                beneficiary.setDob(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_BEN_DOB)));
                beneficiary.setGender(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_BEN_GENDER)));
                beneficiary.setPhoneNumber(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_BEN_PHONE)));
                beneficiary.setCustomerPin(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_BEN_PIN)));
                beneficiary.setBenRefNumber(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_BEN_REF_NO)));
                beneficiary.setPicture(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_BEN_PICTURE)));

                beneficiary.setAltFirstName(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_ALTERNATE_FIRSTNAME)));
                beneficiary.setAltLastName(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_ALTERNATE_LASTNAME)));
                beneficiary.setAltMiddleName(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_ALTERNATE_MIDNAME)));
                beneficiary.setAltDob(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_ALTERNATE_DOB)));
                beneficiary.setAltGender(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_ALTERNATE_GENDER)));
                beneficiary.setAltPhone(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_ALTERNATE_PHONE)));
                beneficiary.setAltRefNumber(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_ALTERNATE_REF_NO)));
                beneficiary.setAltPicture(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_ALTERNATE_PICTURE)));

                beneficiary.setState(String.valueOf(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_STATE))));
                beneficiary.setLga(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_LGA)));
                beneficiary.setCommunity(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_COMMUNITY)));
                beneficiary.setWard(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_WARD)));
                beneficiary.setCity(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_CITY)));
                beneficiary.setAddress(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_ADDRESS)));
                beneficiary.setHouseHoldNum(cursor.getString(cursor.getColumnIndex(Beneficiary.COLUMN_HOUSE_HOLD_NO)));


                beneficiaries.add(beneficiary);
            } while (cursor.moveToNext());
        }

        // close db connection
        cursor.close();
        db.close();

        // return notes list
        return beneficiaries;
    }

    public List<ExportBeneficiary> getAllExportBeneficiaries() {
        List<ExportBeneficiary> beneficiaries = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + ExportBeneficiary.TABLE_BENEFICIARY + " ORDER BY " +
                ExportBeneficiary.COLUMN_BEN_ID + " DESC";

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExportBeneficiary beneficiary = new ExportBeneficiary();
                beneficiary.setCaregiverName(String.valueOf(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_BEN_NAME))));
                beneficiary.setAlternateName(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_ALTERNATE_NAME)));
                beneficiary.setCaregiverGender(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_BEN_GENDER)));
                beneficiary.setAlternateGender(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_ALTERNATE_GENDER)));
                beneficiary.setCaregiverRefNum(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_BEN_REF_NUM)));
                beneficiary.setAlternateRefNum(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_ALTERNATE_REF_NUM)));
                beneficiary.setCaregiverAge(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_BEN_AGE)));
                beneficiary.setAlternateAge(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_ALTERNATE_AGE)));
                beneficiary.setCaregiverPhone(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_BEN_PHONE)));
                beneficiary.setAlternatePhone(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_ALTERNATE_PHONE)));

                beneficiary.setAddress(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_ADDRESS)));
                beneficiary.setState(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_STATE)));
                beneficiary.setCity(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_CITY)));
                beneficiary.setLga(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_LGA)));
                beneficiary.setWard(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_WARD)));
                beneficiary.setCommunity(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_COMMUNITY)));
                beneficiary.setCaregiverBase64Pic(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_BEN_PICTURE)));
                beneficiary.setAlternateBase64Pic(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_ALTERNATE_PICTURE)));
                beneficiary.setHouseHoldNum(String.valueOf(cursor.getString(cursor.getColumnIndex(ExportBeneficiary.COLUMN_HOUSE_HOLD_NO))));


                beneficiaries.add(beneficiary);
            } while (cursor.moveToNext());
        }

        // close db connection
        cursor.close();
        db.close();

        // return notes list
        return beneficiaries;
    }

    public int deleteAllExportBeneficiary() {
        SQLiteDatabase db = this.getWritableDatabase();
        int id = db.delete(ExportBeneficiary.TABLE_BENEFICIARY, null,
                null);
        db.close();
        return id;
    }

    public int deleteAllTransactions() {
        SQLiteDatabase db = this.getWritableDatabase();
        int id = db.delete(Transaction.TABLE_TRANSACTION, null,
                null);
        db.close();
        return id;
    }

    public int getPendingBenCount() {
        String countQuery = "SELECT  * FROM " + ExportBeneficiary.TABLE_BENEFICIARY;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();
        db.close();

        // return count
        return count;
    }
}
