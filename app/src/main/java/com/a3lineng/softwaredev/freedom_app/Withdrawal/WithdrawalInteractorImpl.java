package com.a3lineng.softwaredev.freedom_app.Withdrawal;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.a3lineng.softwaredev.freedom_app.httphelper.Backend;
import com.a3lineng.softwaredev.freedom_app.httphelper.Token;
import com.a3lineng.softwaredev.freedom_app.utilities.TerminalDetails;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class WithdrawalInteractorImpl implements WithdrawalInteractor {
    private WeakReference<Context> contextWeakReference;
    private static final String TAG = WithdrawalInteractorImpl.class.getSimpleName();

    public WithdrawalInteractorImpl(WeakReference<Context> contextWeakReference) {
        this.contextWeakReference = contextWeakReference;
    }

    @Override
    public void withdraw(String accountNumber, String amount, String customerPin, String agentPin, OnWithdrawalFinished listener) {
        if (TextUtils.isEmpty(accountNumber) || TextUtils.isEmpty(amount) || TextUtils.isEmpty(customerPin) || TextUtils.isEmpty(agentPin)) {
            listener.onError("Error", "All fields are required");
        }else{
            if(Utility.isNetworkConnected(contextWeakReference.get())){
                new TransactionToken(listener, accountNumber, amount, customerPin, agentPin, contextWeakReference).execute();
            }else{
                listener.onError("Error", "Internet Connection not available");
            }
        }
    }


    static class TransactionToken extends AsyncTask<Void, Void, String>{
        private WithdrawalInteractor.OnWithdrawalFinished listener;
        String accountNumber, amount, customerPin, agentPin;
        private WeakReference<Context> contextWeakReference;

        public TransactionToken(OnWithdrawalFinished listener, String accountNumber, String amount, String customerPin, String agentPin, WeakReference<Context> contextWeakReference) {
                this.listener = listener;
                this.accountNumber = accountNumber;
                this.amount = amount;
                this.customerPin = customerPin;
                this.agentPin = agentPin;
                this.contextWeakReference = contextWeakReference;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listener.onStart("");
        }

        @Override
        protected String doInBackground(Void... voids) {
            String isoResponse = "Unable to authenticate at this time";
            if(TerminalDetails.isTokenExpired){
                new Token.GetToken(contextWeakReference).execute();
            }
            try {
                isoResponse = Backend.generateTransactionToken(contextWeakReference.get(), accountNumber, customerPin);

            } catch (Exception e) {
                Log.e("ERROR ERROR", Utility.stringify(e));
            }
            return isoResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try{
                if(result != null){
                    JSONObject jsonObject = new JSONObject(result);
                    String respCode = jsonObject.getString("respCode");
                    String respDesc = jsonObject.getString("respDescription");

                    if(respCode.equals("00")){
                        System.out.println(respDesc);
                        listener.onError("Success", respDesc);
                    }else if(respCode.equals("109")){
                        TerminalDetails.isTokenExpired = true;
                        new TransactionToken(listener, accountNumber, amount, customerPin, agentPin, contextWeakReference).execute();
                    }else{
                        listener.onError("Error", "An error occurred, please try again");
                    }
                }
            }catch (Exception e){
                listener.onError("Error", "An error occurred, please try again");
                System.out.println(result);
                Log.e(TAG, Utility.stringify(e));
            }
        }
    }
}
