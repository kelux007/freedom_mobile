package com.a3lineng.softwaredev.freedom_app.model;

public class Schedules {
    public static final String TABLE_SCHEDULES = "schedules";
    public static final String COLUMN_BEN_ID = "id";
    public static final String COLUMN_AMOUNT = "amount";
    public static final String COLUMN_WALLETID = "wallet_id";
    public static final String COLUMN_HOUSEHOLD_NUM = "household_num";
    public static final String COLUMN_BEN_PIN = "ben_pin";
    public static final String CREATE_TABLE_SCHEDULES = "CREATE TABLE " + TABLE_SCHEDULES + "("
            + COLUMN_BEN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_AMOUNT + " TEXT,"
            + COLUMN_WALLETID + " TEXT,"
            + COLUMN_HOUSEHOLD_NUM + " TEXT,"
            + COLUMN_BEN_PIN + " TEXT"
            + ");";
    private ExportBeneficiary exportBeneficiary;
    private String amount;
    private String walletId;
    private String careGiverRefNum;
    private String pin;
    private String houseHoldNum;

    public Schedules(ExportBeneficiary exportBeneficiary, String amount, String walletId, String careGiverRefNum, String pin, String houseHoldNum) {
        this.exportBeneficiary = exportBeneficiary;
        this.amount = amount;
        this.walletId = walletId;
        this.careGiverRefNum = careGiverRefNum;
        this.pin = pin;
        this.houseHoldNum = houseHoldNum;
    }

    public Schedules() {
    }

    public ExportBeneficiary getExportBeneficiary() {
        return exportBeneficiary;
    }

    public void setExportBeneficiary(ExportBeneficiary exportBeneficiary) {
        this.exportBeneficiary = exportBeneficiary;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getCareGiverRefNum() {
        return careGiverRefNum;
    }

    public void setCareGiverRefNum(String careGiverRefNum) {
        this.careGiverRefNum = careGiverRefNum;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getHouseHoldNum() {
        return houseHoldNum;
    }

    public void setHouseHoldNum(String houseHoldNum) {
        this.houseHoldNum = houseHoldNum;
    }

    @Override
    public String toString() {
        return "Schedules{" +
                "exportBeneficiary=" + exportBeneficiary +
                ", amount='" + amount + '\'' +
                ", walletId='" + walletId + '\'' +
                ", careGiverRefNum='" + careGiverRefNum + '\'' +
                '}';
    }
}
