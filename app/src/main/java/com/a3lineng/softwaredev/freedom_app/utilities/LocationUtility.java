package com.a3lineng.softwaredev.freedom_app.utilities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONArray;

import java.util.List;
import java.util.Locale;

import im.delight.android.location.SimpleLocation;

import static android.support.v4.app.ActivityCompat.requestPermissions;
import static android.support.v4.content.ContextCompat.checkSelfPermission;

/**
 * Created by OluwatosinA on 21-Mar-18.
 */

public class LocationUtility {
    public static String lat = "6.4323";
    public static String lon = "3.4123";
    static Context cx;
    static LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            // commented out to prevent overriding GPS's location in cases NETWORKS's location calls this method after GPS's call
            lat = location.getLatitude() + "";
            lon = location.getLongitude() + "";

        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
            Utility.alert(cx, "Alert", "Location disabled, " +
                    "this application needs location access");
            Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            cx.startActivity(i);
        }
    };
    private static Location location;
    private static boolean providersEnabled;
    private static TelephonyManager telephonyManager;

    public static boolean prepareLocationproviders(Context context) {
        cx = context;
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        providersEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!providersEnabled) {
            listener.onProviderDisabled("");
        }
        return providersEnabled;
    }

    public static String getDeviceId(Context context) {
        telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, Contract.PERMISSION_READ_PHONE_STATE);
        }
        return telephonyManager.getDeviceId();
    }

    @SuppressLint("NewApi")
    public static void RequestLocation(Context context) {
        int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);
        int permissionCheck2 = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED && permissionCheck2 != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_FINE_LOCATION)
                    && ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                showExplanation("Permission Needed", "Rationale", new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION}, Contract.ALL_PERMISSIONS_REQUEST);
            } else {
                requestPermission(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        Contract.ALL_PERMISSIONS_REQUEST);
            }
        } else {
            getLocation(context);
        }
    }

    /*
     * alternatively instead of letting listener set the lat and lon we can get the
     * location object directly then get the lat and lon
     * Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
     * lat = location.getLatitude()+"";
     * lon = location.getLongitude()+"";
     */
    private static void showExplanation(String title,
                                        String message,
                                        final String[] permission,
                                        final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cx);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission(permission, permissionRequestCode);
                    }
                });
        builder.create().show();
    }

    /*
     * alternatively instead of letting listener set the lat and lon we can get the
     * location object directly then get the lat and lon
     * Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
     * lat = location.getLatitude()+"";
     * lon = location.getLongitude()+"";
     */

    private static void requestPermission(String[] permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions((Activity) cx,
                permissionName, permissionRequestCode);
    }

    private static void getLocation(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Contract.TIME_BTW_LOCATION_UPDATES, Contract.DISTANCES_BTW_LOCATION_UPDATES, listener);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location != null) {
                lat = location.getLatitude() + "";
                lon = location.getLongitude() + "";
            }
        } else {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, Contract.TIME_BTW_LOCATION_UPDATES, Contract.DISTANCES_BTW_LOCATION_UPDATES, listener);
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                lat = location.getLatitude() + "";
                lon = location.getLongitude() + "";
            } else {
                Log.e("Note Error: ", "There is a problem with location provider");
            }
        }

    }

    public static String getAddress(Context context, SimpleLocation location) {
        try {
            List<Address> addresses;
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());

            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            System.out.println("Location: " + location.getLatitude() + " " + location.getLongitude());
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            return address + " " + city + " " + state;
        } catch (Exception e) {
            Log.e("LocationUtility", Utility.stringify(e));
        }
        return null;
    }

    public static String getStringAddress(JSONArray address) {
        String response = "";
        try{
            String buildingNumber = (String) address.getJSONObject(0).get("short_name");
            String streetName = (String) address.getJSONObject(1).get("short_name");
            String cityName = (String) address.getJSONObject(2).get("short_name");

            response = buildingNumber + ", " + streetName + " " + cityName;

        }catch (Exception e){
            Log.e("LocationUtility", Utility.stringify(e));
        }

        return response;
    }
}