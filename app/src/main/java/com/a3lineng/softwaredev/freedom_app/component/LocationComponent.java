package com.a3lineng.softwaredev.freedom_app.component;

import com.a3lineng.softwaredev.freedom_app.Signin.SigninFragment;
import com.a3lineng.softwaredev.freedom_app.Signup.SignupFragment;
import com.a3lineng.softwaredev.freedom_app.modules.LocationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = LocationModule.class)
public interface LocationComponent {
    void inject(SignupFragment signupFragment);
    void inject(SigninFragment signinFragment);
}
