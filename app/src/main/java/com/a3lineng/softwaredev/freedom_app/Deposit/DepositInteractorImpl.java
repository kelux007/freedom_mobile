package com.a3lineng.softwaredev.freedom_app.Deposit;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.a3lineng.softwaredev.freedom_app.httphelper.Backend;
import com.a3lineng.softwaredev.freedom_app.utilities.TerminalDetails;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

import im.delight.android.location.SimpleLocation;

public class DepositInteractorImpl implements DepositInteractor {
    private WeakReference<Context> contextWeakReference;
    private static final String TAG = DepositInteractorImpl.class.getSimpleName();

    public DepositInteractorImpl(Context context) {
        this.contextWeakReference = new WeakReference<>(context);
    }

    @Override
    public void deposit(String accountNumber, String amount, String narration, String customerPin, String agentPin, DepositInteractor.OnDepositFinishedListener listener) {
        if (TextUtils.isEmpty(accountNumber) || TextUtils.isEmpty(amount) || TextUtils.isEmpty(narration) || TextUtils.isEmpty(customerPin) || TextUtils.isEmpty(agentPin)) {
            listener.onError("Error", "All fields are required");
        } else {
            new Deposit(listener, accountNumber, amount, narration, customerPin, agentPin, contextWeakReference).execute();
        }
    }

    @Override
    public void nameEnquiry(String accountName, SimpleLocation location, DepositInteractor.OnDepositFinishedListener listener) {
        if (!TextUtils.isEmpty(accountName)) {
            new GetAccountName(accountName, contextWeakReference, location, listener).execute();
        } else {
            listener.onError("Error", "Invalid Account Number");
        }
    }

    static class GetAccountName extends AsyncTask<Void, Void, String> {
        private final String accountNumber;
        WeakReference<Context> contextWeakReference;
        SimpleLocation location;
        DepositInteractor.OnDepositFinishedListener listener;

        GetAccountName(String accountNumber, WeakReference<Context> contextWeakReference, SimpleLocation location, DepositInteractor.OnDepositFinishedListener listener) {
            this.accountNumber = accountNumber;
            this.contextWeakReference = contextWeakReference;
            this.location = location;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listener.onStartDeposit("Validating account name ... ");
        }

        @Override
        protected String doInBackground(Void... voids) {
            String isoResponse = "Unable to authenticate at this time";
            if (TerminalDetails.isTokenExpired = true) {
                Backend.getToken(contextWeakReference.get());
            }
            isoResponse = Backend.getName(contextWeakReference.get(), accountNumber, location);
            return isoResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                if (result != null) {
                    JSONObject jsonObject = new JSONObject(result);
                    String respCode = jsonObject.getString("respCode");
                    String respDesc = jsonObject.getString("respDescription");

                    if (respCode.equals("00")) {
                        JSONObject body = jsonObject.getJSONObject("respBody");
                        listener.onNameEnquirySuccess(body.getString("accountName"));
                    } else {
                        listener.onError("Error", respDesc);
                    }

                }
            } catch (Exception ex) {
                Log.e(TAG, Utility.stringify(ex));
                System.out.println(result);
                listener.onError("Error", "Error occurred, please try again");
            }

        }
    }

    static class Deposit extends AsyncTask<Void, Void, String> {
        DepositInteractor.OnDepositFinishedListener listener;
        String accountNumber, amount, narration, customerPin, agentPin;
        WeakReference<Context> contextWeakReference;

        Deposit(DepositInteractor.OnDepositFinishedListener listener, String accountNumber, String amount, String narration, String customerPin, String agentPin, WeakReference<Context> contextWeakReference) {
            this.listener = listener;
            this.accountNumber = accountNumber;
            this.amount = amount;
            this.narration = narration;
            this.customerPin = customerPin;
            this.agentPin = agentPin;
            this.contextWeakReference = contextWeakReference;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listener.onStartDeposit("Processing Deposit Transaction");
        }

        @Override
        protected String doInBackground(Void... voids) {
            String isoResponse = "Unable to authenticate at this time";
            if (TerminalDetails.isTokenExpired = true) {
                Backend.getToken(contextWeakReference.get());
            }
            isoResponse = Backend.deposit(contextWeakReference.get(), accountNumber, amount, narration, customerPin, agentPin);
            return isoResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            final DialogInterface.OnClickListener yeslistener = ((dialog, which) -> {
                dialog.dismiss();
                listener.destroyFragment();
            });
            try {
                if (result != null) {
                    JSONObject jsonObject = new JSONObject(result);
                    String respCode = jsonObject.getString("respCode");
                    String respDescription = jsonObject.getString("respDescription");

                    if (respCode.equals("00")) {
                        listener.onSuccess("Success", respDescription, yeslistener);
                    } else {
                        listener.onError("Error", "Transaction failed, please try again later");
                    }
                } else {
                    listener.onError("Error", "Transaction failed, please try again");
                }
            } catch (Exception ex) {
                listener.onError("Error", "Transaction failed, please try again");
                Log.e(TAG, Utility.stringify(ex));
            }
        }
    }

}
