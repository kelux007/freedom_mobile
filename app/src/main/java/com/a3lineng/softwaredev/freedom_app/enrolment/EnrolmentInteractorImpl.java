package com.a3lineng.softwaredev.freedom_app.enrolment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.a3lineng.softwaredev.freedom_app.Signin.SigninInteractorImpl;
import com.a3lineng.softwaredev.freedom_app.httphelper.Backend;
import com.a3lineng.softwaredev.freedom_app.httphelper.Token;
import com.a3lineng.softwaredev.freedom_app.utilities.TerminalDetails;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class EnrolmentInteractorImpl implements EnrolmentInteractor {
    private WeakReference<Context> contextWeakReference;
    private static final String TAG = EnrolmentInteractorImpl.class.getSimpleName();

    EnrolmentInteractorImpl(WeakReference<Context> contextWeakReference) {
        this.contextWeakReference = contextWeakReference;
    }

    @Override
    public void enrol(String name, String bvn, String motherMaidenName, String dob, String phone, OnEnrolFinished listener) {
        if(TextUtils.isEmpty(name) || TextUtils.isEmpty(bvn) || TextUtils.isEmpty(motherMaidenName) || TextUtils.isEmpty(dob) || TextUtils.isEmpty(phone)){
            listener.onError("Error", "All fields are required");
        }else{
            if(Utility.isNetworkConnected(contextWeakReference.get())){
                new Enrol(name, bvn, motherMaidenName, dob, phone, contextWeakReference, listener).execute();
            }else{
                listener.onError("Error", "Internet connection not available");
            }
        }
    }

    static class Enrol extends AsyncTask<Void, Void, String>{
        private final String name, bvn, motherMaidenName, dob, phone;
        private final WeakReference<Context> contextWeakReference;
        private final EnrolmentInteractor.OnEnrolFinished listener;

        public Enrol(String name, String bvn, String motherMaidenName, String dob, String phone, WeakReference<Context> contextWeakReference, OnEnrolFinished listener) {
            this.name = name;
            this.bvn = bvn;
            this.motherMaidenName = motherMaidenName;
            this.dob = dob;
            this.phone = phone;
            this.contextWeakReference = contextWeakReference;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listener.onStart("");
        }

        @Override
        protected String doInBackground(Void... voids) {
            String isoResponse = "Unable to authenticate at this time";
            if(TerminalDetails.isTokenExpired){
                new Token.GetToken(contextWeakReference).execute();
            }
            try {
                isoResponse = Backend.customerRegistration(contextWeakReference.get(), name, bvn, motherMaidenName, dob, phone);
                if (isoResponse == null) {
                    isoResponse = "Unable to authenticate at this time";
                }
            } catch (Exception e) {
                Log.e("ERROR ERROR", Utility.stringify(e));
            }
            return isoResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            final DialogInterface.OnClickListener yesListener = ((dialog, which) -> {
               listener.destroyFragment();
               dialog.dismiss();
            });
            try{
                if(!result.equalsIgnoreCase("Unable to authenticate at this time")){
                    JSONObject jsonObject = new JSONObject(result);
                    String respCode = jsonObject.getString("respCode");
                    String respDesc = jsonObject.getString("respDescription");

                    if(respCode.equals("000")){
                        listener.onSuccess("Success", respDesc, yesListener);
                    }else if(respCode.equals("109")){
                        TerminalDetails.isTokenExpired = true;
                        new Enrol(name, bvn, motherMaidenName, dob, phone, contextWeakReference, listener).execute();
                    }else{
                        listener.onError("Error", "Error occurred, please try again");
                    }
                }else{
                    listener.onError("Error", "Error occurred, please try again");
                }
            }catch (Exception e){
                Log.e(TAG, Utility.stringify(e));
            }
            listener.onError("Response", result);
        }
    }
}
