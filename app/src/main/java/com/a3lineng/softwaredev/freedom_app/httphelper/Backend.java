package com.a3lineng.softwaredev.freedom_app.httphelper;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.a3lineng.softwaredev.freedom_app.model.Bank;
import com.a3lineng.softwaredev.freedom_app.security.RSACrypt;
import com.a3lineng.softwaredev.freedom_app.security.SecretKeyCrypt;
import com.a3lineng.softwaredev.freedom_app.utilities.Constants;
import com.a3lineng.softwaredev.freedom_app.utilities.LocationUtility;
import com.a3lineng.softwaredev.freedom_app.utilities.SharedPreferencesHelper;
import com.a3lineng.softwaredev.freedom_app.utilities.TerminalDetails;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import im.delight.android.location.SimpleLocation;

public class Backend {


    public static String agentSetup(Context context, String username, String newAuthData, String authData, String password, String confirmpassword) {

        String response = "Unable to authenticate at this time";

        try {
            Gson gson = null;
            String http_verb = Constants.HTTP_POST;
            String timestamp = String.format("%d", System.currentTimeMillis() / 1000L);
            String nonce = Utility.randomNumber(30);
            authData = RSACrypt.encrypt(authData, context);
            newAuthData = RSACrypt.encrypt(newAuthData, context);
            String deviceid = Utility.getDeviceId(context);
            String stringToBeSigned = http_verb + "&" + timestamp + "&" + nonce + "&" + username + "&" + deviceid;
            System.out.println("String to be signed: " + stringToBeSigned);
            String signature = Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP);
            Map headers = new HashMap<String, Object>();

            gson = new Gson();

            headers.put("timestamp", timestamp);
            headers.put("nonce", nonce);
            headers.put("signature", signature);
            headers.put("Content-Type", "text/plain");


            JSONObject body = new JSONObject();
            body.put("username", username);
            body.put("authData", authData);
            body.put("newAuthData", newAuthData);
            body.put("password", password);
            body.put("deviceId", deviceid);


            System.out.println("Authorization :" + headers.get("authorization"));
            System.out.println("Unencrypted body " + body.toString());
            System.out.println("Encrypted body " + SecretKeyCrypt.encrypt(gson.toJson(body)));

            response = HttpHelper.get(TerminalDetails.BASE_URL + TerminalDetails.gravityBasePath + TerminalDetails.agentsetup, SecretKeyCrypt.encrypt(body.toString()), Constants.HTTP_POST, headers);

            System.out.println("Response received " + response + "\nResponse decrypted " + SecretKeyCrypt.decrypt(response));
            response = SecretKeyCrypt.decrypt(response);
        } catch (Exception ex) {
            Log.e("ERROR ", Utility.stringify(ex));
        }
        return response;
    }

    public static String getName(Context context, String accountNumber, SimpleLocation location) {
        String response = "Unable to authenticate at this time";

        try {
            String client_id = SharedPreferencesHelper.getStringValue(context, "clientId", SharedPreferencesHelper.PREF);
            String shared_secret_key = SharedPreferencesHelper.getStringValue(context, "shared_secret_key", SharedPreferencesHelper.PREF);

            Gson gson = null;
            String http_verb = Constants.HTTP_POST;
            String timestamp = String.format("%d", System.currentTimeMillis() / 1000L);
            String nonce = Utility.randomNumber(30);
            String deviceid = Utility.getDeviceId(context);
            String stringToBeSigned = http_verb + "&" + timestamp + "&" + nonce + "&" + client_id + "&" + shared_secret_key + "&"
                    + accountNumber + "&" +  LocationUtility.lat + "&" + LocationUtility.lon + "&" + deviceid;
            System.out.println("String to be signed: " + stringToBeSigned);
            String signature = Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP);
            Map<String, String> headers = new HashMap<String, String>();

            gson = new Gson();

            headers.put("timestamp", timestamp);
            headers.put("nonce", nonce);
            headers.put("authorization", "Bearer " + TerminalDetails.OAuth_Token);
            headers.put("signature", Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP));
            headers.put("Content-Type", "text/plain");


            JSONObject body = new JSONObject();
            body.put("accountNo", accountNumber);
            body.put("deviceId", deviceid);
            body.put("latitude", LocationUtility.lat);
            body.put("longitude", LocationUtility.lon);
            body.put("bankCode", "02");


            System.out.println("Authorization : " + headers.get("authorization"));
            System.out.println("Unencrypted body " + body.toString());
            System.out.println("Encrypted body " + SecretKeyCrypt.encrypt(gson.toJson(body)));

            response = HttpHelper.get(TerminalDetails.BASE_URL + TerminalDetails.gravityBasePath + TerminalDetails.verifyName, SecretKeyCrypt.encrypt(body.toString()), Constants.HTTP_POST, headers);
            System.out.println("Response received " + response + "\nResponse decrypted " + SecretKeyCrypt.decrypt(response));
            response = SecretKeyCrypt.decrypt(response);
        } catch (Exception ex) {
            Log.e("ERROR ", Utility.stringify(ex));
        }
        return response;
    }

    public static String deposit(Context context, String accountNumber, String amount, String narration, String customerPin, String agentPin) {
        String response = "Unable to authenticate at this time";

        try {
            String client_id = SharedPreferencesHelper.getStringValue(context, "clientId", SharedPreferencesHelper.PREF);
            String shared_secret_key = SharedPreferencesHelper.getStringValue(context, "shared_secret_key", SharedPreferencesHelper.PREF);

            Gson gson = null;
            String http_verb = Constants.HTTP_POST;
            String timestamp = String.format("%d", System.currentTimeMillis() / 1000L);
            String nonce = Utility.randomNumber(30);
            agentPin = RSACrypt.encrypt(agentPin, context);
            String deviceid = Utility.getDeviceId(context);
            String stringToBeSigned = http_verb + "&" + timestamp + "&" + nonce + "&" + client_id + "&" + shared_secret_key + "&"
                    + accountNumber + "&" + amount + "&" + LocationUtility.lat + "&" + LocationUtility.lon + "&" + deviceid + "&"
                    + narration + "/" + customerPin + "&" + "1";
            System.out.println("String to be signed: " + stringToBeSigned);
            String signature = Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP);
            Map<String, String> headers = new HashMap<String, String>();

            gson = new Gson();

            headers.put("timestamp", timestamp);
            headers.put("nonce", nonce);
            headers.put("authorization", "Bearer " + TerminalDetails.OAuth_Token);
            headers.put("signature", Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP));
            headers.put("Content-Type", "text/plain");


            JSONObject body = new JSONObject();
            body.put("authData", agentPin);
            body.put("customerId", accountNumber);
            body.put("productId", "1");
            body.put("deviceId", deviceid);
            body.put("latitude", LocationUtility.lat);
            body.put("longitude", LocationUtility.lon);
            body.put("bankCode", "02");
            body.put("amount", amount);
            body.put("narration", narration + "/" + customerPin);


            System.out.println("Authorization : " + headers.get("authorization"));
            System.out.println("Unencrypted body " + body.toString());
            System.out.println("Encrypted body " + SecretKeyCrypt.encrypt(gson.toJson(body)));

            response = HttpHelper.get(TerminalDetails.BASE_URL + TerminalDetails.gravityBasePath + TerminalDetails.deposit, SecretKeyCrypt.encrypt(body.toString()), Constants.HTTP_POST, headers);
            System.out.println("Response received " + response + "\nResponse decrypted " + SecretKeyCrypt.decrypt(response));
            response = SecretKeyCrypt.decrypt(response);
        } catch (Exception ex) {
            Log.e("ERROR ", Utility.stringify(ex));
        }
        return response;
    }

    public static String withdrawal(Context context, String otp, String accountNumber, String amount, String pin, String agentPin) {
        String response = "Unable to authenticate at this time";

        try {
            String client_id = SharedPreferencesHelper.getStringValue(context, "clientId", SharedPreferencesHelper.PREF);
            String shared_secret_key = SharedPreferencesHelper.getStringValue(context, "shared_secret_key", SharedPreferencesHelper.PREF);

            Gson gson = null;
            String http_verb = Constants.HTTP_POST;
            String timestamp = String.format("%d", System.currentTimeMillis() / 1000L);
            String nonce = Utility.randomNumber(30);
            agentPin = RSACrypt.encrypt(agentPin, context);
            String deviceid = Utility.getDeviceId(context);
            String stringToBeSigned = http_verb + "&" + timestamp + "&" + nonce + "&" + client_id + "&" + shared_secret_key + "&"
                    + accountNumber + "&" + amount + "&" + LocationUtility.lat + "&" + LocationUtility.lon + "&" + deviceid + "&"
                    + "&" + "1" + "&" + otp;
            System.out.println("String to be signed: " + stringToBeSigned);
            String signature = Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP);
            Map<String, String> headers = new HashMap<String, String>();

            gson = new Gson();

            headers.put("timestamp", timestamp);
            headers.put("nonce", nonce);
            headers.put("authorization", "Bearer " + TerminalDetails.OAuth_Token);
            headers.put("signature", Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP));
            headers.put("Content-Type", "text/plain");


            JSONObject body = new JSONObject();
            body.put("authData", agentPin);
            body.put("customerPin", RSACrypt.encrypt(pin, context));
            body.put("customerId", accountNumber);
            body.put("productId", "1");
            body.put("deviceId", deviceid);
            body.put("latitude", LocationUtility.lat);
            body.put("longitude", LocationUtility.lon);
            body.put("bankCode", "02");
            body.put("amount", amount);
            body.put("otp", otp);
//            body.put("narration", narration + "_" + receiptNumber);


            System.out.println("Authorization : " + headers.get("authorization"));
            System.out.println("Unencrypted body " + body.toString());
            System.out.println("Encrypted body " + SecretKeyCrypt.encrypt(gson.toJson(body)));

            response = HttpHelper.get(TerminalDetails.BASE_URL + TerminalDetails.gravityBasePath + TerminalDetails.withdrawal, SecretKeyCrypt.encrypt(body.toString()), Constants.HTTP_POST, headers);
            System.out.println("Response received " + response + "\nResponse decrypted " + SecretKeyCrypt.decrypt(response));
            response = SecretKeyCrypt.decrypt(response);
        } catch (Exception ex) {
            Log.e("ERROR ", Utility.stringify(ex));
        }
        return response;
    }


    public static String logon(Context context, String username, String password) {
        String response = "Unable to authenticate at this time";

        try {
            String client_id = SharedPreferencesHelper.getStringValue(context, "clientId", SharedPreferencesHelper.PREF);
            String shared_secret_key = SharedPreferencesHelper.getStringValue(context, "shared_secret_key", SharedPreferencesHelper.PREF);

            Gson gson = null;
            String http_verb = Constants.HTTP_POST;
            String timestamp = String.format("%d", System.currentTimeMillis() / 1000L);
            String nonce = Utility.randomNumber(30);
            String deviceid = Utility.getDeviceId(context);
            String stringToBeSigned = http_verb + "&" + timestamp + "&" + nonce + "&" + client_id + "&" + shared_secret_key;
            System.out.println("String to be signed: " + stringToBeSigned);
            String signature = Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP);
            Map<String, String> headers = new HashMap<String, String>();

            gson = new Gson();

            headers.put("timestamp", timestamp);
            headers.put("nonce", nonce);
            headers.put("authorization", "Bearer " + TerminalDetails.OAuth_Token);
            headers.put("signature", Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP));
            headers.put("Content-Type", "text/plain");


            JSONObject body = new JSONObject();
            body.put("username", username);
            body.put("deviceId", deviceid);
            body.put("authData", RSACrypt.encrypt(password, context));


            System.out.println("Authorization :" + headers.get("authorization"));
            System.out.println("Unencrypted body " + body.toString());
            System.out.println("Encrypted body " + SecretKeyCrypt.encrypt(gson.toJson(body)));

            response = HttpHelper.get(TerminalDetails.BASE_URL + TerminalDetails.gravityBasePath + TerminalDetails.agentLogon, SecretKeyCrypt.encrypt(body.toString()), Constants.HTTP_POST, headers);

            System.out.println("Response received " + response + "\nResponse decrypted " + SecretKeyCrypt.decrypt(response));
        } catch (Exception ex) {
            Log.e("ERROR ", Utility.stringify(ex));
        }
        return response;
    }

    public static String getToken(Context context) {
        String response = "";
        try {
            Gson gson = null;
            String body = "grant_type=client_credentials";
            String http_verb = Constants.HTTP_POST;
            String timestamp = String.format("%d", System.currentTimeMillis() / 1000L);
            String nonce = Utility.randomNumber(30);

            String deviceid = Utility.getDeviceId(context);
            String stringToBeSigned = http_verb + "&" + timestamp + "&" + nonce + "&" + deviceid;


            String signature = Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP);
            String client_id = SharedPreferencesHelper.getStringValue(context, "clientId", SharedPreferencesHelper.PREF);
            String shared_secret_key = SharedPreferencesHelper.getStringValue(context, "shared_secret_key", SharedPreferencesHelper.PREF);
            System.out.println("String to be signed: " + client_id + " " + shared_secret_key);
            //cliend_id = "eb163727917cbba1eea208541a643e74";

            String authorization = Base64.encodeToString((client_id + ":" + shared_secret_key).getBytes(), Base64.NO_WRAP);
            Map<String, String> headers = new HashMap<String, String>();


            //headers.put("timestamp", timestamp);
            //headers.put("nonce", nonce);
            //headers.put("signature", signature);
            headers.put("authorization", "Basic " + Base64.encodeToString((client_id + ":" + shared_secret_key).getBytes("UTF-8"), Base64.NO_WRAP));
            headers.put("Content-Type", "application/x-www-form-urlencoded");

            System.out.println("Authorization : " + Base64.encodeToString((client_id + ":" + shared_secret_key).getBytes("UTF-8"), Base64.NO_WRAP));
            System.out.println("Unencrypted Body: " + body);
            System.out.println("Encrypted Body: " + SecretKeyCrypt.encrypt(body));

            response = HttpHelper.get(TerminalDetails.BASE_URL + TerminalDetails.gravityBasePath + TerminalDetails.oauth, SecretKeyCrypt.encrypt(body), "POST", headers);

            try {

                String result = SecretKeyCrypt.decrypt(response);
                JSONObject jsonObject = new JSONObject(result);
                String respCode = jsonObject.getString("respCode");
                String respDesc = jsonObject.getString("respDescription");
                if (respCode.equals("00")) {
                    TerminalDetails.isTokenExpired = false;
                    TerminalDetails.token_expires_in = jsonObject.getString("expires_in");
                    TerminalDetails.OAuth_Token = jsonObject.getString("access_token");
                }
            } catch (Exception e) {
            }

            System.out.println("Auth response: " + response);
        } catch (Exception ex) {
            Log.e("Backend Error: ", Utility.stringify(ex));

        }

        return response;
    }

    public static String customerRegistration(Context context, String name, String bvn, String motherMaidenName, String dob, String phone) {
        String response = "Unable to authenticate at this time";

        try {
            String client_id = SharedPreferencesHelper.getStringValue(context, "clientId", SharedPreferencesHelper.PREF);
            String shared_secret_key = SharedPreferencesHelper.getStringValue(context, "shared_secret_key", SharedPreferencesHelper.PREF);

            Gson gson = null;
            String http_verb = Constants.HTTP_POST;
            String timestamp = String.format("%d", System.currentTimeMillis() / 1000L);
            String nonce = Utility.randomNumber(30);
            String deviceid = Utility.getDeviceId(context);
            String stringToBeSigned = http_verb + "&" + timestamp + "&" + nonce + "&" + client_id + "&" + shared_secret_key + "&"
                     + deviceid;
            System.out.println("String to be signed: " + stringToBeSigned);
            String signature = Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP);
            Map<String, String> headers = new HashMap<String, String>();

            gson = new Gson();

            headers.put("timestamp", timestamp);
            headers.put("nonce", nonce);
            headers.put("authorization", "Bearer " + TerminalDetails.OAuth_Token);
            headers.put("signature", Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP));
            headers.put("Content-Type", "text/plain");


            JSONObject body = new JSONObject();
            body.put("name", name);
            body.put("bvn", bvn);
            body.put("mothersMaidenName", motherMaidenName);
            body.put("dateOfBrth", dob);
            body.put("phoneNumber", phone);

            System.out.println("Authorization : " + headers.get("authorization"));
            System.out.println("Unencrypted body " + body.toString());
            System.out.println("Encrypted body " + SecretKeyCrypt.encrypt(gson.toJson(body)));

            response = HttpHelper.get(TerminalDetails.BASE_URL + TerminalDetails.gravityBasePath + TerminalDetails.customerRegistration, SecretKeyCrypt.encrypt(body.toString()), Constants.HTTP_POST, headers);
            System.out.println("Response received " + response + "\nResponse decrypted " + SecretKeyCrypt.decrypt(response));
            response = SecretKeyCrypt.decrypt(response);
        } catch (Exception ex) {
            Log.e("ERROR ", Utility.stringify(ex));
        }
        return response;
    }

    public static List<Bank> getAllBanks(Context context, String bvn, String pin, String motherMaidenName) {
        String response = "Unable to authenticate at this time";

        try {
            String client_id = SharedPreferencesHelper.getStringValue(context, "clientId", SharedPreferencesHelper.PREF);
            String shared_secret_key = SharedPreferencesHelper.getStringValue(context, "shared_secret_key", SharedPreferencesHelper.PREF);

            Gson gson = null;
            String http_verb = Constants.HTTP_POST;
            String timestamp = String.format("%d", System.currentTimeMillis() / 1000L);
            String nonce = Utility.randomNumber(30);
            String deviceid = Utility.getDeviceId(context);
            String stringToBeSigned = http_verb + "&" + timestamp + "&" + nonce + "&" + client_id + "&" + shared_secret_key + "&"
                    + deviceid;
            System.out.println("String to be signed: " + stringToBeSigned);
            String signature = Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP);
            Map<String, String> headers = new HashMap<String, String>();

            gson = new Gson();

            headers.put("timestamp", timestamp);
            headers.put("nonce", nonce);
            headers.put("authorization", "Bearer " + TerminalDetails.OAuth_Token);
            headers.put("signature", Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP));
            headers.put("Content-Type", "text/plain");


            JSONObject body = new JSONObject();
            body.put("pin", RSACrypt.encrypt(pin, context));
            body.put("bvn", bvn);
            body.put("mothersMaidenName", motherMaidenName);


            System.out.println("Authorization : " + headers.get("authorization"));
            System.out.println("Unencrypted body " + body.toString());
            System.out.println("Encrypted body " + SecretKeyCrypt.encrypt(gson.toJson(body)));

            response = HttpHelper.get(TerminalDetails.BASE_URL + TerminalDetails.gravityBasePath + TerminalDetails.generateTransactionToken, SecretKeyCrypt.encrypt(body.toString()), Constants.HTTP_POST, headers);
            System.out.println("Response received " + response + "\nResponse decrypted " + SecretKeyCrypt.decrypt(response));
            response = SecretKeyCrypt.decrypt(response);
            //List<Bank> banks = gson.fromJson(response, Bank.class);
        } catch (Exception ex) {
            Log.e("ERROR ", Utility.stringify(ex));
        }
        return null;
    }

    public static String generateTransactionToken(Context context, String accountNumber, String pin) {
        String response = "Unable to authenticate at this time";

        try {
            String client_id = SharedPreferencesHelper.getStringValue(context, "clientId", SharedPreferencesHelper.PREF);
            String shared_secret_key = SharedPreferencesHelper.getStringValue(context, "shared_secret_key", SharedPreferencesHelper.PREF);

            Gson gson = null;
            String http_verb = Constants.HTTP_POST;
            String timestamp = String.format("%d", System.currentTimeMillis() / 1000L);
            String nonce = Utility.randomNumber(30);
            String deviceid = Utility.getDeviceId(context);
            String stringToBeSigned = http_verb + "&" + timestamp + "&" + nonce + "&" + client_id + "&" + shared_secret_key + "&"
                    + deviceid;
            System.out.println("String to be signed: " + stringToBeSigned);
            String signature = Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP);
            Map<String, String> headers = new HashMap<String, String>();

            gson = new Gson();

            headers.put("timestamp", timestamp);
            headers.put("nonce", nonce);
            headers.put("authorization", "Bearer " + TerminalDetails.OAuth_Token);
            headers.put("signature", Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP));
            headers.put("Content-Type", "text/plain");


            JSONObject body = new JSONObject();
            body.put("pin", RSACrypt.encrypt(pin, context));
            body.put("accountNumber", accountNumber);


            System.out.println("Authorization : " + headers.get("authorization"));
            System.out.println("Unencrypted body " + body.toString());
            System.out.println("Encrypted body " + SecretKeyCrypt.encrypt(gson.toJson(body)));

            response = HttpHelper.get(TerminalDetails.BASE_URL + TerminalDetails.gravityBasePath + TerminalDetails.generateTransactionToken, SecretKeyCrypt.encrypt(body.toString()), Constants.HTTP_POST, headers);
            System.out.println("Response received " + response + "\nResponse decrypted " + SecretKeyCrypt.decrypt(response));
            response = SecretKeyCrypt.decrypt(response);
        } catch (Exception ex) {
            Log.e("ERROR ", Utility.stringify(ex));
        }
        return response;
    }

    public static String intraBankTransfer(Context context, String bankCode, String sourceAccount, String destinationAccount, String amount, String narration, String customerPin, String agentPin, String receiptNumber) {
        String response = "Unable to authenticate at this time";

        try {
            String client_id = SharedPreferencesHelper.getStringValue(context, "clientId", SharedPreferencesHelper.PREF);
            String shared_secret_key = SharedPreferencesHelper.getStringValue(context, "shared_secret_key", SharedPreferencesHelper.PREF);
            String streetName = SharedPreferencesHelper.getStringValue(context, "location", SharedPreferencesHelper.PREF);

            Gson gson = null;
            String http_verb = Constants.HTTP_POST;
            String timestamp = String.format("%d", System.currentTimeMillis() / 1000L);
            String nonce = Utility.randomNumber(30);
            agentPin = RSACrypt.encrypt(agentPin, context);
            String deviceid = Utility.getDeviceId(context);
            String stringToBeSigned = http_verb + "&" + timestamp + "&" + nonce + "&" + client_id + "&" + shared_secret_key + "&"
                    + sourceAccount  + "&" + LocationUtility.lat + "&" + LocationUtility.lon + "&" + deviceid + "&"
                    + destinationAccount + "&" + amount + "&" + (narration + "_" + receiptNumber);
            System.out.println("String to be signed: " + stringToBeSigned);
            String signature = Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP);
            Map<String, String> headers = new HashMap<String, String>();

            gson = new Gson();

            headers.put("timestamp", timestamp);
            headers.put("nonce", nonce);
            headers.put("clientid", client_id);
            headers.put("username", TerminalDetails.beneficiary);
            headers.put("authorization", "Bearer " + TerminalDetails.OAuth_Token);
            headers.put("signature", Base64.encodeToString(Utility.sha512Byte(stringToBeSigned), Base64.NO_WRAP));
            headers.put("Content-Type", "text/plain");


            JSONObject body = new JSONObject();
            body.put("authData", agentPin);
            body.put("amount", amount);
            body.put("customerPin", RSACrypt.encrypt(customerPin, context));
            body.put("sourceAccount", sourceAccount);
            body.put("destinationAccount", destinationAccount);
            body.put("deviceId", deviceid);
            body.put("latitude", LocationUtility.lat);
            body.put("longitude", LocationUtility.lon);
            body.put("bankCode", "02");
            body.put("productId", "1");
            body.put("channel", "MobileApp");
            body.put("merchantLocation", streetName);
            body.put("naration", (narration + "_" + receiptNumber));


            System.out.println("Authorization : " + headers.get("authorization"));
            System.out.println("Unencrypted body " + body.toString());
            System.out.println("Encrypted body " + SecretKeyCrypt.encrypt(gson.toJson(body)));

            response = HttpHelper.get(TerminalDetails.BASE_URL + TerminalDetails.gravityBasePath + TerminalDetails.intraBankTransfer, SecretKeyCrypt.encrypt(body.toString()), Constants.HTTP_POST, headers);
            System.out.println("Response received " + response + "\nResponse decrypted " + SecretKeyCrypt.decrypt(response));
            response = SecretKeyCrypt.decrypt(response);
        } catch (Exception ex) {
            Log.e("ERROR ", Utility.stringify(ex));
        }
        return response;
    }
}
