package com.a3lineng.softwaredev.freedom_app.model;

public class Beneficiary {
    public static final String TABLE_BENEFICIARY = "beneficiaries";
    public static final String COLUMN_BEN_ID = "id";
    public static final String COLUMN_BEN_FIRSTNAME = "firstname";
    public static final String COLUMN_BEN_LASTNAME = "lastname";
    public static final String COLUMN_BEN_MIDDLENAME = "middlename";
    public static final String COLUMN_BEN_GENDER = "gender";
    public static final String COLUMN_BEN_PHONE = "phone";
    public static final String COLUMN_BEN_DOB = "dob";
    public static final String COLUMN_BEN_PIN = "pin";
    public static final String COLUMN_BEN_REF_NO = "ben_ref_no";
    public static final String COLUMN_ALTERNATE_FIRSTNAME = "alt_firstname";
    public static final String COLUMN_ALTERNATE_LASTNAME = "alt_lastname";
    public static final String COLUMN_ALTERNATE_MIDNAME = "alt_middlename";
    public static final String COLUMN_ALTERNATE_DOB = "alt_dob";
    public static final String COLUMN_ALTERNATE_GENDER = "alt_gender";
    public static final String COLUMN_ALTERNATE_PHONE = "alt_phone";
    public static final String COLUMN_ALTERNATE_REF_NO = "alt_ref_no";
    public static final String COLUMN_LGA = "lga";
    public static final String COLUMN_STATE = "state";
    public static final String COLUMN_WARD = "ward";
    public static final String COLUMN_COMMUNITY = "community";
    public static final String COLUMN_BEN_PICTURE = "ben_pic";
    public static final String COLUMN_ALTERNATE_PICTURE = "alt_pic";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_CITY = "city";
    public static final String COLUMN_HOUSE_HOLD_NO = "house_hold_no";
    public static final String CREATE_TABLE_BENEFICIARY = "CREATE TABLE " + TABLE_BENEFICIARY + "("
            + COLUMN_BEN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_BEN_FIRSTNAME + " TEXT,"
            + COLUMN_BEN_LASTNAME + " TEXT,"
            + COLUMN_BEN_MIDDLENAME + " TEXT,"
            + COLUMN_BEN_DOB + " DATETIME,"
            + COLUMN_BEN_GENDER + " TEXT,"
            + COLUMN_BEN_PHONE + " TEXT,"
            + COLUMN_BEN_PIN + " TEXT,"
            + COLUMN_BEN_PICTURE + " TEXT,"
            + COLUMN_BEN_REF_NO + " TEXT,"
            + COLUMN_ALTERNATE_FIRSTNAME + " TEXT,"
            + COLUMN_ALTERNATE_LASTNAME + " TEXT,"
            + COLUMN_ALTERNATE_MIDNAME + " TEXT,"
            + COLUMN_ALTERNATE_DOB + " DATETIME,"
            + COLUMN_ALTERNATE_GENDER + " TEXT,"
            + COLUMN_ALTERNATE_PHONE + " TEXT,"
            + COLUMN_ALTERNATE_PICTURE + " TEXT,"
            + COLUMN_ALTERNATE_REF_NO + " TEXT,"
            + COLUMN_ADDRESS + " TEXT,"
            + COLUMN_CITY + " TEXT,"
            + COLUMN_HOUSE_HOLD_NO + " TEXT,"
            + COLUMN_STATE + " TEXT,"
            + COLUMN_LGA + " TEXT,"
            + COLUMN_WARD + " TEXT,"
            + COLUMN_COMMUNITY + " TEXT"
            + ");";
    private String id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String dob;
    private String gender;
    private String phoneNumber;
    private String customerPin;
    private String altFirstName;
    private String altLastName;
    private String altMiddleName;
    private String altDob;
    private String altGender;
    private String altPhone;
    private String state;
    private String lga;
    private String ward;
    private String community;
    private String picture;
    private String altPicture;
    private String address;
    private String benRefNumber;
    private String altRefNumber;
    private String houseHoldNum;
    private String city;


    public Beneficiary() {
    }

    public Beneficiary(String id, String firstName, String lastName, String middleName, String dob, String gender, String phoneNumber, String customerPin, String altFirstName, String altLastName, String altMiddleName, String altDob, String altGender, String altPhone, String state, String lga, String ward, String community, String picture, String altPicture, String address, String benRefNumber, String altRefNumber, String houseHoldNum, String city) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.dob = dob;
        this.gender = gender;
        this.phoneNumber = phoneNumber;
        this.customerPin = customerPin;
        this.altFirstName = altFirstName;
        this.altLastName = altLastName;
        this.altMiddleName = altMiddleName;
        this.altDob = altDob;
        this.altGender = altGender;
        this.altPhone = altPhone;
        this.state = state;
        this.lga = lga;
        this.ward = ward;
        this.community = community;
        this.picture = picture;
        this.altPicture = altPicture;
        this.address = address;
        this.benRefNumber = benRefNumber;
        this.altRefNumber = altRefNumber;
        this.houseHoldNum = houseHoldNum;
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCustomerPin() {
        return customerPin;
    }

    public void setCustomerPin(String customerPin) {
        this.customerPin = customerPin;
    }

    public String getAltFirstName() {
        return altFirstName;
    }

    public void setAltFirstName(String altFirstName) {
        this.altFirstName = altFirstName;
    }

    public String getAltLastName() {
        return altLastName;
    }

    public void setAltLastName(String altLastName) {
        this.altLastName = altLastName;
    }

    public String getAltMiddleName() {
        return altMiddleName;
    }

    public void setAltMiddleName(String altMiddleName) {
        this.altMiddleName = altMiddleName;
    }

    public String getAltDob() {
        return altDob;
    }

    public void setAltDob(String altDob) {
        this.altDob = altDob;
    }

    public String getAltGender() {
        return altGender;
    }

    public void setAltGender(String altGender) {
        this.altGender = altGender;
    }

    public String getAltPhone() {
        return altPhone;
    }

    public void setAltPhone(String altPhone) {
        this.altPhone = altPhone;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLga() {
        return lga;
    }

    public void setLga(String lga) {
        this.lga = lga;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getAltPicture() {
        return altPicture;
    }

    public void setAltPicture(String altPicture) {
        this.altPicture = altPicture;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBenRefNumber() {
        return benRefNumber;
    }

    public void setBenRefNumber(String benRefNumber) {
        this.benRefNumber = benRefNumber;
    }

    public String getAltRefNumber() {
        return altRefNumber;
    }

    public void setAltRefNumber(String altRefNumber) {
        this.altRefNumber = altRefNumber;
    }

    public String getHouseHoldNum() {
        return houseHoldNum;
    }

    public void setHouseHoldNum(String houseHoldNum) {
        this.houseHoldNum = houseHoldNum;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "TransactionView{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", dob='" + dob + '\'' +
                ", gender='" + gender + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", customerPin='" + customerPin + '\'' +
                ", altFirstName='" + altFirstName + '\'' +
                ", altLastName='" + altLastName + '\'' +
                ", altMiddleName='" + altMiddleName + '\'' +
                ", altDob='" + altDob + '\'' +
                ", altGender='" + altGender + '\'' +
                ", altPhone='" + altPhone + '\'' +
                ", state='" + state + '\'' +
                ", lga='" + lga + '\'' +
                ", ward='" + ward + '\'' +
                ", community='" + community + '\'' +
                ", picture='" + picture + '\'' +
                ", altPicture='" + altPicture + '\'' +
                ", address='" + address + '\'' +
                ", benRefNumber='" + benRefNumber + '\'' +
                ", altRefNumber='" + altRefNumber + '\'' +
                ", houseHoldNum='" + houseHoldNum + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
