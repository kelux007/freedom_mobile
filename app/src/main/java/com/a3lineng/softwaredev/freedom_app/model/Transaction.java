package com.a3lineng.softwaredev.freedom_app.model;

public class Transaction {
    public static final String TABLE_TRANSACTION = "transactions";
    public static final String COLUMN_TRANSACTION_ID = "id";
    public static final String COLUMN_HOUSEHOLD_NUM = "house_hold_num";
    public static final String COLUMN_AMOUNT = "amount";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_COLLECTEDBY = "collected_by";
    public static final String COLUMN_AGENT_PIN = "agent_pin";
    public static final String COLUMN_PIN = "pin";
    public static final String CREATE_TABLE_TRANSACTION = "CREATE TABLE " + TABLE_TRANSACTION + "("
            + COLUMN_TRANSACTION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_HOUSEHOLD_NUM + " TEXT,"
            + COLUMN_AMOUNT + " TEXT,"
            + COLUMN_LONGITUDE + " TEXT,"
            + COLUMN_LATITUDE + " TEXT,"
            + COLUMN_COLLECTEDBY + " TEXT,"
            + COLUMN_AGENT_PIN + " TEXT,"
            + COLUMN_PIN + " TEXT"
            + ");";
    private String houseHoldNum;
    private String amount;
    private String longitude;
    private String latitude;
    private String collectedBy;
    private String agent;
    private String pin;

    public Transaction() {
    }

    public Transaction(String houseHoldNum, String amount, String longitude, String latitude, String collectedBy, String agent, String pin) {
        this.houseHoldNum = houseHoldNum;
        this.amount = amount;
        this.longitude = longitude;
        this.latitude = latitude;
        this.collectedBy = collectedBy;
        this.agent = agent;
        this.pin = pin;
    }

    public String getHouseHoldNum() {
        return houseHoldNum;
    }

    public void setHouseHoldNum(String houseHoldNum) {
        this.houseHoldNum = houseHoldNum;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getCollectedBy() {
        return collectedBy;
    }

    public void setCollectedBy(String collectedBy) {
        this.collectedBy = collectedBy;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "houseHoldNum='" + houseHoldNum + '\'' +
                ", amount='" + amount + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", collectedBy='" + collectedBy + '\'' +
                ", agent='" + agent + '\'' +
                ", pin='" + pin + '\'' +
                '}';
    }
}
