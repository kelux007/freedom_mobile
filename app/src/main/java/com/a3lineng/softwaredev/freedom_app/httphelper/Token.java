package com.a3lineng.softwaredev.freedom_app.httphelper;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.a3lineng.softwaredev.freedom_app.enrolment.EnrolmentInteractorImpl;
import com.a3lineng.softwaredev.freedom_app.security.CryptographyException;
import com.a3lineng.softwaredev.freedom_app.security.SecretKeyCrypt;
import com.a3lineng.softwaredev.freedom_app.utilities.TerminalDetails;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class Token {

    public static class GetToken extends AsyncTask<String, String, String> {
        private final WeakReference<Context> contextWeakReference;
        private static final String TAG = EnrolmentInteractorImpl.class.getSimpleName();

        public GetToken(WeakReference<Context> contextWeakReference) {
            this.contextWeakReference = contextWeakReference;
        }

        @Override
        protected String doInBackground(String... strings) {
            String isoResponse = "Unable to authenticate at this time";

            try {
                isoResponse = Backend.getToken(contextWeakReference.get());
                if (isoResponse == null) {
                    isoResponse = "Unable to authenticate at this time";
                }
            } catch (Exception e) {
                Log.e("ERROR ERROR", Utility.stringify(e));
            }
            return isoResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("SERVER RESPONSE", result);
            try {
                result = SecretKeyCrypt.decrypt(result);
                Log.e("\nSR decrypted ", result);
            } catch (CryptographyException e1) {
                e1.printStackTrace();
            }
            JSONObject responseIso = null;
            try {
                responseIso = new JSONObject(result);
            } catch (Exception e) {
                Log.e(TAG, Utility.stringify(e));
            }


            String respCode = "";
            String respDesc = "";

            try {
                respCode = responseIso.getString("respCode");
                respDesc = responseIso.getString("respDescription");
                if (respCode.equals("00")) {
                    TerminalDetails.isTokenExpired = false;
                    TerminalDetails.token_expires_in = responseIso.getString("expires_in");
                    TerminalDetails.OAuth_Token = responseIso.getString("access_token");
                }
            } catch (Exception e) {
                Log.e(TAG, Utility.stringify(e));
            }
        }

    }
}
