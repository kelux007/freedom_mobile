package com.a3lineng.softwaredev.freedom_app.Deposit;

import android.content.DialogInterface;

public interface DepositView {
    void showProgress(String message);

    void hideProgress();

    void destroyFragment();

    void updateAccountName(String accountName);

    void showAlert(String title, String message);

    void updateProgress(String message);

    void showAlert2(String title, String message, DialogInterface.OnClickListener yeslistener, DialogInterface.OnClickListener nolistener);

    void showAlert4(String title, String message, DialogInterface.OnClickListener yeslistener);

    void showAlert5(String title, String message, DialogInterface.OnClickListener cancel, DialogInterface.OnClickListener retry);

    void showAlert3(String title, String message, DialogInterface.OnClickListener yeslistener, DialogInterface.OnClickListener nolistener);
}

