package com.a3lineng.softwaredev.freedom_app.listeners;

import android.support.v4.app.Fragment;

public interface OnDestroyFragmentRequest {
    void onFragmentDestroy(Fragment fragment);
}