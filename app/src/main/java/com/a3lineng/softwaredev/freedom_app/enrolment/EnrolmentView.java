package com.a3lineng.softwaredev.freedom_app.enrolment;

import android.content.DialogInterface;

import com.a3lineng.softwaredev.freedom_app.base_interactors.BaseView;

public interface EnrolmentView extends BaseView {
    void destroyFragment();
    void alert(String title, String message, DialogInterface.OnClickListener yeslistener);
}
