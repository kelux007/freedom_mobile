package com.a3lineng.softwaredev.freedom_app.TabView_Fragments;



import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.a3lineng.softwaredev.freedom_app.R;
import com.a3lineng.softwaredev.freedom_app.model.ExportBeneficiary;
import com.a3lineng.softwaredev.freedom_app.recyclerviews.TransactionViewAdapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AccountsTabFragment extends Fragment {
    private RecyclerView recyclerView;
    private TransactionViewAdapter mAdapter;
    private WeakReference<Context> contextWeakReference;
    private List<ExportBeneficiary> beneficiaryList = new ArrayList<>();

    public AccountsTabFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_accounts_tab, container, false);
        contextWeakReference = new WeakReference<>(getContext());
        initializeViews(view);
        mAdapter = new TransactionViewAdapter(beneficiaryList);

        //recyclerView.setHasFixedSize(true);

        // vertical RecyclerView
        // keep beneficiary_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(contextWeakReference.get());

        // horizontal RecyclerView
        // keep beneficiary_list_row.xml width to `wrap_content`
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

//        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
//
//        // adding custom divider line with padding 16dp
//        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);

        // row click listener

//        prepareBeneficiaryData();
        return view;
    }

    private void initializeViews(View view) {
        recyclerView = view.findViewById(R.id.recycler_view);
    }

    /**
     * Prepares sample data to provide data set to adapter
     */
//    private void prepareBeneficiaryData() {
//        DBHelper db = new DBHelper(contextWeakReference.get());
//        beneficiaryList.addAll(db.getAllExportBeneficiaries());
//        //System.out.println("Note Size: " + beneficiaryList.size() + " " + beneficiaryList.get(1).toString());
//        // notify adapter about data set changes
//        // so that it will render the list with new data
//        mAdapter.notifyDataSetChanged();
//    }


}
