package com.a3lineng.softwaredev.freedom_app.utilities;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.StringTokenizer;

public class NumberTextWatcherForThousand implements TextWatcher {
    private EditText editText;
    private MaterialBetterSpinner materialDesignSpinner;
    private Button btnProceed;
    private long delay = 1000; // 1 seconds after user stops typing
    private long last_text_edit = 0;
    private Handler handler = new Handler();
//    private Runnable input_finish_checker = new Runnable() {
//        public void run() {
//            if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {
//                // TODO: do what you need here
//                // ............
//                // ............
//                editText.setEnabled(false);
//                materialDesignSpinner.setEnabled(false);
//                btnProceed.setEnabled(true);
//                btnProceed.setClickable(true);
//                Toast.makeText(AssetApplication.getAppContext(), trimCommaOfString(editText.getText().toString().trim()), Toast.LENGTH_LONG).show();
//            }
//        }
//    };

    public NumberTextWatcherForThousand(EditText editText, Button btnProceed, MaterialBetterSpinner materialDesignSpinner) {
        this.editText = editText;
        this.btnProceed = btnProceed;
        this.materialDesignSpinner = materialDesignSpinner;
    }

    private static String getDecimalFormattedString(String value) {
        StringTokenizer lst = new StringTokenizer(value, ".");
        String str1 = value;
        String str2 = "";
        if (lst.countTokens() > 1) {
            str1 = lst.nextToken();
            str2 = lst.nextToken();
        }
        String str3 = "";
        int i = 0;
        int j = -1 + str1.length();
        if (str1.charAt(-1 + str1.length()) == '.') {
            j--;
            str3 = ".";
        }
        for (int k = j; ; k--) {
            if (k < 0) {
                if (str2.length() > 0)
                    str3 = str3 + "." + str2;
                return str3;
            }
            if (i == 3) {
                str3 = "," + str3;
                i = 0;
            }
            str3 = str1.charAt(k) + str3;
            i++;
        }
    }

    public static String trimCommaOfString(String string) {
//        String returnString;
        if (string.contains(",")) {
            return string.replace(",", "");
        } else {
            return string;
        }

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        //You need to remove this to run only once
        //handler.removeCallbacks(input_finish_checker);
    }

    @Override
    public void afterTextChanged(Editable editable) {
        //avoid triggering event when text is empty
        if (editable.length() >= 3) {
            //last_text_edit = System.currentTimeMillis();
            //handler.postDelayed(input_finish_checker, delay);
            btnProceed.setEnabled(true);
            btnProceed.setEnabled(true);
            btnProceed.setClickable(true);
        }

        try {
            editText.removeTextChangedListener(this);
            String value = editText.getText().toString();


            if (value != null && !value.equals("")) {

//            if(value.startsWith(".")){
//                editText.setText("0.");
//            }
//            if(value.startsWith("0") && !value.startsWith("0.")){
//                editText.setText("");
//
//            }


                String str = editText.getText().toString().replaceAll(",", "");
                if (!value.equals(""))
                    editText.setText(getDecimalFormattedString(str));
                editText.setSelection(editText.getText().toString().length());
            }
            editText.addTextChangedListener(this);
            return;
        } catch (Exception ex) {
            ex.printStackTrace();
            editText.addTextChangedListener(this);
        }
    }
}
