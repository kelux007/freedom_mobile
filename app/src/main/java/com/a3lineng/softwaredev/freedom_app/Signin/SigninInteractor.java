package com.a3lineng.softwaredev.freedom_app.Signin;

import com.a3lineng.softwaredev.freedom_app.base_interactors.BaseFinishedListener;
import com.a3lineng.softwaredev.freedom_app.base_interactors.BaseInteractor;

import im.delight.android.location.SimpleLocation;

public interface SigninInteractor extends BaseInteractor {
    void signinAgent(String username, String password, SimpleLocation location, OnSigninFinished listener);

    interface OnSigninFinished extends BaseFinishedListener {

    }
}
