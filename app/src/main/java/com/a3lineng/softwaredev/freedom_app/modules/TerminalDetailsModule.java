package com.a3lineng.softwaredev.freedom_app.modules;

import android.content.Context;

import com.a3lineng.softwaredev.freedom_app.httphelper.HttpHelper;
import com.a3lineng.softwaredev.freedom_app.utilities.TerminalDetails;
import com.a3lineng.softwaredev.freedom_app.utilities.Utility;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class TerminalDetailsModule {
    private Context context;

    public TerminalDetailsModule(Context context) {
        this.context = context;
    }

//    @Provides
//    @Singleton
//    Context provideContext() {
//        return this.context;
//    }

    @Provides
    @Singleton
    TerminalDetails provideTerminalDetails() {
        return new TerminalDetails();
    }

    @Provides
    @Singleton
    HttpHelper provideHttpHelper() {
        return new HttpHelper();
    }

    @Provides
    @Singleton
    Utility provideUtility() {
        return new Utility();
    }
}
