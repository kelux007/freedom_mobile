package com.a3lineng.softwaredev.freedom_app.Signin;

import com.a3lineng.softwaredev.freedom_app.base_interactors.BaseView;

public interface SigninView extends BaseView{

    void navigateToHomeActivity();

}
