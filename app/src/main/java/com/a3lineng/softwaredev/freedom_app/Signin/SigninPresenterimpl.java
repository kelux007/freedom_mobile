package com.a3lineng.softwaredev.freedom_app.Signin;

import im.delight.android.location.SimpleLocation;

public class SigninPresenterimpl implements SigninPresenter, SigninInteractor.OnSigninFinished{
    private SigninView signinView;
    private SigninInteractor signinInteractor;

    public SigninPresenterimpl(SigninView signinView, SigninInteractor signinInteractor) {
        this.signinView = signinView;
        this.signinInteractor = signinInteractor;
    }

    @Override
    public void validateLoginCredentials(String username, String password, SimpleLocation location) {
        if(signinView != null){
            signinInteractor.signinAgent(username, password, location, this);
        }
    }

    @Override
    public void onDestroy() {
        if(signinView != null){
            signinView.hideProgress();
            signinView = null;
            signinInteractor = null;
        }
    }

    @Override
    public void onError(String title, String message) {
        if(signinView != null){
            signinView.hideProgress();
            signinView.alert(title, message);
        }
    }

    @Override
    public void onSuccess() {
        if(signinView != null){
            signinView.hideProgress();
            signinView.navigateToHomeActivity();
        }
    }

    @Override
    public void onUpdateProgressMessage(String message) {

    }

    @Override
    public void onStart(String message) {
        if(signinView != null){
            signinView.showProgress(message);
        }
    }
}
