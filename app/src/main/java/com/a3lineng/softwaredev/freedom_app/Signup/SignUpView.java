package com.a3lineng.softwaredev.freedom_app.Signup;

import com.a3lineng.softwaredev.freedom_app.base_interactors.BaseView;

public interface SignUpView extends BaseView{
    void navigateToLogin();
}
