package com.a3lineng.softwaredev.freedom_app.listeners;

public interface OnSettingsMenuClicked {
    void launchSettings();
}
