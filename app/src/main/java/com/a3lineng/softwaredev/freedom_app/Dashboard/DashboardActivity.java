package com.a3lineng.softwaredev.freedom_app.Dashboard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;

import com.a3lineng.softwaredev.freedom_app.Account_Opening.AccountOpeningFragment;
import com.a3lineng.softwaredev.freedom_app.Balance_Equiry.BalanceEnquiryFragment;
import com.a3lineng.softwaredev.freedom_app.Bill_Payment.BillPaymentFragment;
import com.a3lineng.softwaredev.freedom_app.Change_Password.PasswordChange;
import com.a3lineng.softwaredev.freedom_app.Change_Pin.PinChange;
import com.a3lineng.softwaredev.freedom_app.ContactsAdapter;
import com.a3lineng.softwaredev.freedom_app.Deposit.DepositFragment;
import com.a3lineng.softwaredev.freedom_app.R;
import com.a3lineng.softwaredev.freedom_app.Signin.SignInActivity;
import com.a3lineng.softwaredev.freedom_app.Transfer.TransferFragment;
import com.a3lineng.softwaredev.freedom_app.Withdrawal.WithdrawalFragment;
import com.a3lineng.softwaredev.freedom_app.enrolment.EnrolmentFragment;
import com.a3lineng.softwaredev.freedom_app.listeners.OnDestroyFragmentRequest;
import com.a3lineng.softwaredev.freedom_app.utilities.SharedPreferencesHelper;

import java.lang.ref.WeakReference;


public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnDestroyFragmentRequest {

    private WeakReference<Context> contextWeakReference;
    private SearchView searchView;
    private ContactsAdapter mAdapter;
    private TextView tvAgentName, tvAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        contextWeakReference = new WeakReference<>(getBaseContext());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState != null) {
            getFragmentManager().executePendingTransactions();
            android.app.Fragment fragmentById = getFragmentManager().findFragmentById(R.id.fragment_container);


            if (fragmentById != null) {
                getFragmentManager().beginTransaction().remove(fragmentById).commit();
            }
        }

        HomeFragment homeFragment = new HomeFragment();
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.fragment_container, homeFragment, "home").commit();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        tvAgentName = navigationView.getHeaderView(0).findViewById(R.id.textViewAgentname);
        tvAddress = navigationView.getHeaderView(0).findViewById(R.id.textViewLocation);

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        getMenuInflater().inflate(R.menu.dashoard, menu);


//        // Associate searchable configuration with the SearchView
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        searchView = (SearchView) menu.findItem(R.id.action_search)
//                .getActionView();
//        searchView.setSearchableInfo(searchManager
//                .getSearchableInfo(getComponentName()));
//        searchView.setMaxWidth(Integer.MAX_VALUE);
//
//        // listening to search query text change
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                // filter recycler view when query submitted
//                mAdapter.getFilter().filter(query);
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String query) {
//                // filter recycler view when text is changed
//                mAdapter.getFilter().filter(query);
//                return false;
//            }
//        });
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            HomeFragment homeFragment = new HomeFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, homeFragment).commit();

        } else if (id == R.id.nav_acct_open) {
            AccountOpeningFragment accountOpeningFragment = new AccountOpeningFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, accountOpeningFragment).commit();
        } else if (id == R.id.nav_enroll) {
            EnrolmentFragment enrolmentFragment = new EnrolmentFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, enrolmentFragment).commit();

        } else if (id == R.id.nav_bal_enq) {
            BalanceEnquiryFragment balanceEnquiryFragment = new BalanceEnquiryFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, balanceEnquiryFragment).commit();

        } else if (id == R.id.nav_withdrawal) {
            WithdrawalFragment withdrawalFragment = new WithdrawalFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, withdrawalFragment).commit();
        }
//        } else if (id == R.id.nav_accountWithdrawal) {
//            WithdrawalFragment withdrawalFragment = new WithdrawalFragment();
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction().replace(R.id.fragment_container, withdrawalFragment).commit();
//
//        }
        else if (id == R.id.nav_deposit) {
            DepositFragment depositFragment = new DepositFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, depositFragment).commit();

        } else if (id == R.id.nav_trans) {
            TransferFragment transferFragment = new TransferFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, transferFragment).commit();


        } else if (id == R.id.nav_pay_bill) {
            BillPaymentFragment billPaymentFragment = new BillPaymentFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, billPaymentFragment).commit();

        } else if (id == R.id.nav_change_pass) {
            PasswordChange passwordChangeFragment = new PasswordChange();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, passwordChangeFragment).commit();

        } else if (id == R.id.nav_change_pin) {
            PinChange pinChangeFragment = new PinChange();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, pinChangeFragment).commit();
        } else if (id == R.id.nav_logout) {

            Intent intent = new Intent(contextWeakReference.get(), SignInActivity.class);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void populateViews() {
        String agentName = SharedPreferencesHelper.getStringValue(contextWeakReference.get(), "account_name", SharedPreferencesHelper.PREF);
        String agentBalance = SharedPreferencesHelper.getStringValue(contextWeakReference.get(), "agent_balance", SharedPreferencesHelper.PREF);
        String location = SharedPreferencesHelper.getStringValue(contextWeakReference.get(), "location", SharedPreferencesHelper.PREF);

        tvAgentName.setText(agentName);
        //textViewagentbalance.setText(Utility.parseAmout(agentBalance));
        tvAddress.setText(location);
    }

    @Override
    public void onFragmentDestroy(Fragment fragment) {
//        // Create fragment and give it an argument for the selected article
//        Dashboard newFragment = new Dashboard();
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
//        // Replace whatever is in the fragment_container view with this fragment,
//        // and add the transaction to the back stack so the user can navigate back
//        transaction.replace(R.id.fragment_container, newFragment);
//        transaction.addToBackStack(null);
//
//        // Commit the transaction
//        transaction.commit();
    }
}
