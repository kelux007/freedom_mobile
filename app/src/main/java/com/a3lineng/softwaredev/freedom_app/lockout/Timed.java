package com.a3lineng.softwaredev.freedom_app.lockout;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.a3lineng.softwaredev.freedom_app.utilities.TerminalDetails;

import java.lang.ref.WeakReference;


/**
 * Created by OluwatosinA on 16-Mar-18.
 */

public class Timed extends AppCompatActivity {
    Runnable runnable;
    Handler handler;
    private WeakReference<Context> contextWeakReference;
    private WeakReference<Activity> activityWeakReference;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contextWeakReference = new WeakReference<Context>(this);
        activityWeakReference = new WeakReference<Activity>(this);
        TerminalDetails.saveBooleanValue(activityWeakReference.get(), TerminalDetails.PREF, "logged_in", true);
//        if(!TerminalDetails.getUserSession(activityWeakReference.get(), "logged_in")){
//            Intent intent = new Intent(this, LockScreenActivity.class);
//            //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//        }
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                //redirect user to login screen
                TerminalDetails.saveBooleanValue(activityWeakReference.get(), TerminalDetails.PREF, "logged_in", false);

            }
        };
        stopHandler();
        startHandler();
        System.out.println("NOTE: Setting logged in value to false in onCreate");
    }

    private void startHandler() {
        handler.postDelayed(runnable, 300000);
    }

    private void stopHandler() {
        handler.removeCallbacks(runnable);
    }


    @Override
    protected void onPause() {
        super.onPause();
        //stopHandler();
        //startHandler();
        System.out.println("Main Activity onPause called");
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (!TerminalDetails.getUserSession(this, "logged_in")) {
////            stopHandler();
////            System.out.println("Handler stopped in the onResume");
//            //Intent intent = new Intent(this, LockScreenActivity.class);
//            //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            //startActivity(intent);
//        }else {
//            startHandler();
//        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (!TerminalDetails.getUserSession(this, "logged_in")) {

        } else {
            stopHandler();
            System.out.println("Handler stopped");
            startHandler();
            System.out.println("Handler started");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }
}
