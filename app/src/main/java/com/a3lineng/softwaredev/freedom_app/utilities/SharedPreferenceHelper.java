package com.a3lineng.softwaredev.freedom_app.utilities;//package com.a3line.softwaredev.nipostmobile.utilities;

import android.content.SharedPreferences;

import javax.inject.Inject;

public class SharedPreferenceHelper {
    private SharedPreferences sharedPreferences;

    @Inject
    public SharedPreferenceHelper(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void putStringData(String key, String data) {
        sharedPreferences.edit().putString(key, data).apply();
    }

    public String getStringData(String key) {
        return sharedPreferences.getString(key, "");
    }
}
