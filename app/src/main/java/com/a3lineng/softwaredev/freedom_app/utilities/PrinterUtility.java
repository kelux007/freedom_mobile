//package com.a3lineng.softwaredev.freedom_app.utilities;
//
//import android.content.Context;
//import android.content.DialogInterface;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.util.Log;
//
//import com.a3line.softwaredev.nipostmobile.AssetApplication;
//import com.a3line.softwaredev.nipostmobile.R;
//import com.a3line.softwaredev.nipostmobile.model.ExportBeneficiary;
//
//import java.util.Date;
//
//import justtide.ThermalPrinter;
//
//public class PrinterUtility {
//
//    static ThermalPrinter thermalPrinter = ThermalPrinter.getInstance();
//
//    public static int printRegistrationReceipt(Context context, ExportBeneficiary beneficiary) {
//
//        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//                printRegistrationReceipt(context, beneficiary);
//                dialog.dismiss();
//            }
//        };
//
//        Bitmap bitmap1 = null;
//        Bitmap bitmapPicture1 = null;
//        Bitmap bitmapPicture2 = null;
//        Bitmap qrCode = null;
//        byte[] buffer = new byte[100];
//        int i, state;
//        boolean blnPrinterStatus = false;
//        thermalPrinter.initBuffer();
//        blnPrinterStatus = thermalPrinter.isOverTemperature() || thermalPrinter.isPaperOut();
//
//        if (blnPrinterStatus) {
//            Utility.printAlert(context, "Error", "Cannot perform Print Operation, Check Paper or Temperature", listener, true);
//            return -1;
//        }
//
//        try {
//            bitmap1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.ncto_alerticon);
//
////            bitmapPicture1 = Utility.getBitmap(beneficiary.getCaregiverBase64Pic());
////            bitmapPicture2 = Utility.getBitmap(beneficiary.getAlternateBase64Pic());
//            qrCode = Utility.textToImage(beneficiary.getCaregiverPhone(), 260, 260);
//
//        } catch (Exception e) {
//            Log.e("Error: ", Utility.stringify(e));
//        }
//
//
//        thermalPrinter.printLogo(0, 14, bitmap1);
//
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.setGray(3);
//        thermalPrinter.setHeightAndLeft(0, 0);
//        thermalPrinter.setLineSpacing(5);
//        thermalPrinter.setDispMode(ThermalPrinter.UMODE);
//        //thermalPrinter.setFont(ThermalPrinter.ASC8X16_DEF, ThermalPrinter.HZK12);
//        thermalPrinter.setFont(ThermalPrinter.ASC12X24, ThermalPrinter.HZK12);
//        thermalPrinter.setStep(12);
//        for (i = 0; i < 96; i++)
//            buffer[i] = (byte) 0xfc;
//
//        thermalPrinter.printLine(buffer);
//        thermalPrinter.setStep(12);
//        thermalPrinter.print(" CARE GIVER \n");
//        thermalPrinter.setStep(12);
//        thermalPrinter.printLine(buffer);
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("Name: " + beneficiary.getCaregiverName() + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("Age: " + beneficiary.getCaregiverAge() + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("Phone: " + beneficiary.getCaregiverPhone() + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.printLine(buffer);
//        thermalPrinter.setStep(12);
//        thermalPrinter.print(" ALTERNATE \n");
//        thermalPrinter.setStep(12);
//        thermalPrinter.printLine(buffer);
//
//        thermalPrinter.print("Name: " + beneficiary.getAlternateName() + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("Age: " + beneficiary.getAlternateAge() + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("Phone: " + beneficiary.getAlternatePhone() + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.printLine(buffer);
//        thermalPrinter.setStep(12);
//        thermalPrinter.print(" LOCATION \n");
//        thermalPrinter.setStep(12);
//        thermalPrinter.printLine(buffer);
//
//        thermalPrinter.print("Community: " + beneficiary.getCommunity() + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("Ward: " + beneficiary.getWard() + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("LGA: " + beneficiary.getLga() + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("STATE: " + beneficiary.getState() + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("Timestamp: " + new Date() + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.printLogo(0, 4, qrCode);
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print(AssetApplication.getAppContext().getResources().getString(R.string.copy_right_text) + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.printLine(buffer);
//        thermalPrinter.setStep(200);
//        thermalPrinter.printStart();
//        state = thermalPrinter.waitForPrintFinish();
//        return state;
//
//    }
//
//    public static int printDisbursementReceipt(Context context) {
//
//        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//                printDisbursementReceipt(context);
//                dialog.dismiss();
//            }
//        };
//
//        Bitmap bitmap1 = null;
//        byte[] buffer = new byte[100];
//        int i, state;
//
//        boolean blnPrinterStatus = false;
//        thermalPrinter.initBuffer();
//
//        blnPrinterStatus = thermalPrinter.isOverTemperature() || thermalPrinter.isPaperOut();
//
//        if (blnPrinterStatus) {
//            Utility.printAlert(context, "Error", "Cannot perform Print Operation, Check Paper or Temperature", listener, true);
//            return -1;
//        }
//
//        try {
//            bitmap1 = BitmapFactory.decodeResource(AssetApplication.getAppContext().getResources(), R.drawable.ncto_alerticon);
//
//        } catch (Exception e) {
//            Log.e("Error: ", Utility.stringify(e));
//        }
//
//        thermalPrinter.printLogo(0, 14, bitmap1);
//
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.setGray(3);
//        thermalPrinter.setHeightAndLeft(0, 0);
//        thermalPrinter.setLineSpacing(5);
//        thermalPrinter.setDispMode(ThermalPrinter.UMODE);
//        //thermalPrinter.setFont(ThermalPrinter.ASC8X16_DEF, ThermalPrinter.HZK12);
//        thermalPrinter.setFont(ThermalPrinter.ASC12X24, ThermalPrinter.HZK12);
//        thermalPrinter.setStep(12);
//        for (i = 0; i < 96; i++)
//            buffer[i] = (byte) 0xfc;
//
//        thermalPrinter.printLine(buffer);
//        thermalPrinter.setStep(12);
//        thermalPrinter.print(" CASH DISBURSEMENT \n");
//        thermalPrinter.setStep(12);
//        thermalPrinter.printLine(buffer);
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("Household Number: " + ApplicationInstance.currentPrintBuffer.get("houseHoldNum") + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("Amount: " + ApplicationInstance.currentPrintBuffer.get("amount") + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("collectedBy: " + ApplicationInstance.currentPrintBuffer.get("collectedBy") + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("Transaction Status: " + ApplicationInstance.currentPrintBuffer.get("status") + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.print("Timestamp: " + new Date() + "\n");
//        thermalPrinter.setStep(12);
//
//        thermalPrinter.printLine(buffer);
//        thermalPrinter.setStep(200);
//        thermalPrinter.printStart();
//        state = thermalPrinter.waitForPrintFinish();
//        return state;
//    }
//}