package com.a3lineng.softwaredev.freedom_app.httphelper;

import org.json.JSONObject;

/**
 * Created by OluwatosinA on 26-Mar-18.
 */

public interface WebSocketResult {

    void onMessageReceived(JSONObject result);


}
