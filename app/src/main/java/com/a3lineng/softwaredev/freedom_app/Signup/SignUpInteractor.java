package com.a3lineng.softwaredev.freedom_app.Signup;

import com.a3lineng.softwaredev.freedom_app.base_interactors.BaseFinishedListener;

public interface SignUpInteractor {
    void signupAgent(String username, String authData, String newAuthData, String password, String confirmpassword, OnSignupFinishedListener listener);

    interface OnSignupFinishedListener extends BaseFinishedListener{

    }
}
