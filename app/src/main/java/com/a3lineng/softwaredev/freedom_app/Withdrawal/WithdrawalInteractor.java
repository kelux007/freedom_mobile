package com.a3lineng.softwaredev.freedom_app.Withdrawal;

import android.content.DialogInterface;

import com.a3lineng.softwaredev.freedom_app.base_interactors.BaseFinishedListener;

public interface WithdrawalInteractor {

    void withdraw(String accountNumber, String amount, String customerPin, String agentPin, OnWithdrawalFinished listener);

    interface OnWithdrawalFinished extends BaseFinishedListener {
        void onSuccess(String title, String message, DialogInterface.OnClickListener yesListener);
    }
}
