package com.a3lineng.softwaredev.freedom_app.security;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import android.app.Activity;
import android.content.Context;

import java.io.InputStream;


/**
 * @author OlalekanW
 */
public class RSACrypt {
    private static final String transformation = "RSA/ECB/PKCS1Padding";
    private static final String encoding = "UTF-8";

    static {
    }

    public static String decrypt(String cipher, Context context) throws Exception {
        RSACipher rsaCipher = new RSACipher();
        InputStream privateKey = ((Activity) context).getAssets().open("private.key");
        return rsaCipher.decrypt(cipher, privateKey, transformation, encoding);
    }

    public static String encrypt(String clear, Context context) throws Exception {
        RSACipher rsaCipher = new RSACipher();
        InputStream publicKey = ((Activity) context).getAssets().open("public.key");
        return rsaCipher.encrypt(clear, publicKey, transformation, encoding);
    }
}
